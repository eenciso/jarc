﻿namespace Facturacion
{
    partial class frmPrincipalFacturacion
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipalFacturacion));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolDescargaFac = new System.Windows.Forms.ToolStripButton();
            this.toolStripFacturas = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnBusEmitidas = new System.Windows.Forms.ToolStripMenuItem();
            this.btnusRec = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTablas = new System.Windows.Forms.ToolStripButton();
            this.toolStripCatCont = new System.Windows.Forms.ToolStripButton();
            this.toolStripCalculos = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnMensuales = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCalcImpuestos = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnCodigoInterno = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolDescargaFac,
            this.toolStripFacturas,
            this.toolStripTablas,
            this.toolStripCatCont,
            this.toolStripCalculos,
            this.toolStripButton1,
            this.toolStripDropDownButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(960, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolDescargaFac
            // 
            this.toolDescargaFac.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDescargaFac.Image = ((System.Drawing.Image)(resources.GetObject("toolDescargaFac.Image")));
            this.toolDescargaFac.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDescargaFac.Name = "toolDescargaFac";
            this.toolDescargaFac.Size = new System.Drawing.Size(23, 22);
            this.toolDescargaFac.Text = "Descarga de Facturas";
            this.toolDescargaFac.Click += new System.EventHandler(this.toolDescargaFac_Click);
            // 
            // toolStripFacturas
            // 
            this.toolStripFacturas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripFacturas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBusEmitidas,
            this.btnusRec});
            this.toolStripFacturas.Image = ((System.Drawing.Image)(resources.GetObject("toolStripFacturas.Image")));
            this.toolStripFacturas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripFacturas.Name = "toolStripFacturas";
            this.toolStripFacturas.Size = new System.Drawing.Size(29, 22);
            this.toolStripFacturas.Text = "Consulta de facturas";
            // 
            // btnBusEmitidas
            // 
            this.btnBusEmitidas.Image = ((System.Drawing.Image)(resources.GetObject("btnBusEmitidas.Image")));
            this.btnBusEmitidas.Name = "btnBusEmitidas";
            this.btnBusEmitidas.Size = new System.Drawing.Size(226, 22);
            this.btnBusEmitidas.Text = "Busqueda Facturas Emitidas";
            this.btnBusEmitidas.Click += new System.EventHandler(this.btnBusEmitidas_Click);
            // 
            // btnusRec
            // 
            this.btnusRec.Image = ((System.Drawing.Image)(resources.GetObject("btnusRec.Image")));
            this.btnusRec.Name = "btnusRec";
            this.btnusRec.Size = new System.Drawing.Size(226, 22);
            this.btnusRec.Text = "Busqueda Facturas Recibidas";
            this.btnusRec.Click += new System.EventHandler(this.btnusRec_Click);
            // 
            // toolStripTablas
            // 
            this.toolStripTablas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripTablas.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTablas.Image")));
            this.toolStripTablas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTablas.Name = "toolStripTablas";
            this.toolStripTablas.Size = new System.Drawing.Size(23, 22);
            this.toolStripTablas.Text = "Tablas";
            this.toolStripTablas.Click += new System.EventHandler(this.toolStripTablas_Click);
            // 
            // toolStripCatCont
            // 
            this.toolStripCatCont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCatCont.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCatCont.Image")));
            this.toolStripCatCont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCatCont.Name = "toolStripCatCont";
            this.toolStripCatCont.Size = new System.Drawing.Size(23, 22);
            this.toolStripCatCont.Text = "Catalogo de Contabilidad";
            this.toolStripCatCont.Click += new System.EventHandler(this.toolStripCatCont_Click);
            // 
            // toolStripCalculos
            // 
            this.toolStripCalculos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCalculos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMensuales,
            this.btnCalcImpuestos});
            this.toolStripCalculos.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCalculos.Image")));
            this.toolStripCalculos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCalculos.Name = "toolStripCalculos";
            this.toolStripCalculos.Size = new System.Drawing.Size(29, 22);
            this.toolStripCalculos.Text = "Consulta de facturas";
            // 
            // btnMensuales
            // 
            this.btnMensuales.Name = "btnMensuales";
            this.btnMensuales.Size = new System.Drawing.Size(229, 22);
            this.btnMensuales.Text = "Ingresos y Egresos Mensuales";
            this.btnMensuales.Click += new System.EventHandler(this.btnMensuales_Click);
            // 
            // btnCalcImpuestos
            // 
            this.btnCalcImpuestos.Image = ((System.Drawing.Image)(resources.GetObject("btnCalcImpuestos.Image")));
            this.btnCalcImpuestos.Name = "btnCalcImpuestos";
            this.btnCalcImpuestos.Size = new System.Drawing.Size(229, 22);
            this.btnCalcImpuestos.Text = "Calculo de Impuestos";
            this.btnCalcImpuestos.Click += new System.EventHandler(this.btnCalcImpuestos_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCodigoInterno});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // btnCodigoInterno
            // 
            this.btnCodigoInterno.Name = "btnCodigoInterno";
            this.btnCodigoInterno.Size = new System.Drawing.Size(208, 22);
            this.btnCodigoInterno.Text = "Catalogo Código Interino";
            this.btnCodigoInterno.Click += new System.EventHandler(this.btnCodigoInterno_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // frmPrincipalFacturacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(960, 344);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.Name = "frmPrincipalFacturacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CONCENTRADO DE FACTURAS ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolDescargaFac;
        private System.Windows.Forms.ToolStripButton toolStripTablas;
        private System.Windows.Forms.ToolStripDropDownButton toolStripFacturas;
        private System.Windows.Forms.ToolStripButton toolStripCatCont;
        private System.Windows.Forms.ToolStripMenuItem btnBusEmitidas;
        private System.Windows.Forms.ToolStripMenuItem btnusRec;
        private System.Windows.Forms.ToolStripDropDownButton toolStripCalculos;
        private System.Windows.Forms.ToolStripMenuItem btnCalcImpuestos;
        private System.Windows.Forms.ToolStripMenuItem btnMensuales;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem btnCodigoInterno;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}

