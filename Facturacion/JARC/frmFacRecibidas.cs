﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Facturacion.JARC
{
    public partial class frmFacRecibidas : Form
    {
        string strRutas = string.Empty;
        string strAnioGeneral = string.Empty;
        string strMesGeneral = string.Empty;
        string strRutaGeneeral = string.Empty;
        private String rutaXML;
        Factura facturaVo = new Factura();
        String strRuta = string.Empty;
        MySqlConnection conectar;
        string strUUID = string.Empty;
        string strConse = string.Empty;
        string sValue = string.Empty;
        string sValueMat = string.Empty;
        int selectedIndexs = 0;
        public frmFacRecibidas(string strRuta, string strUUID1, string strConsecutivo, string strAnio, string strMes)
        {
            strAnioGeneral = strAnio;
            strMesGeneral = strMes;
            strRutaGeneeral = strRuta;
            strUUID = strUUID1;
            strRutas = strRuta;
            strConse = strConsecutivo;
            ConexionBD();
            InitializeComponent();
            Abrir();
        }

        private void Abrir()
        {
            //rutaXML = "C:\\Users\\Jazmin\\OneDrive\\Documentos\\Facturas_Recibidas\\" + strRutas;
            rutaXML = "C:\\CapDigi\\CFDiDescargaXml\\Recibidos\\RACA490321B88\\" + strRutas;
            XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
            XmlTextReader reader = new XmlTextReader(@"" + rutaXML + "");
            Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
            facturaVo.DatosFactura = factura;

            ExtraeComprobante();
            ExtraeDatosEmisor();
            ExtraeConceptos();
        }

        private void ExtraeComprobante()
        {

            string version;
            string sello;
            string serie;
            string folio;
            DateTime fecha;
            string formaDePago;
            string noCertificado;
            string certificado;
            decimal subTotal;
            decimal descuento;
            decimal total;
            string metodoDePago;
            string tipodeComprobante;
            string moneda;

            version = facturaVo.DatosFactura.Version;
            sello = facturaVo.DatosFactura.Sello;
            serie = facturaVo.DatosFactura.Serie;
            folio = facturaVo.DatosFactura.Folio;
            fecha = facturaVo.DatosFactura.Fecha;
            formaDePago = facturaVo.DatosFactura.FormaPagoSpecified.ToString();
            noCertificado = facturaVo.DatosFactura.NoCertificado;
            certificado = facturaVo.DatosFactura.Certificado;
            subTotal = facturaVo.DatosFactura.SubTotal;
            descuento = facturaVo.DatosFactura.Descuento;
            total = facturaVo.DatosFactura.Total;
            metodoDePago = facturaVo.DatosFactura.MetodoPagoSpecified.ToString();
            tipodeComprobante = facturaVo.DatosFactura.TipoDeComprobante.ToString();
            moneda = facturaVo.DatosFactura.Moneda.ToString();
            this.textConsecutivo.Text = strConse;
            AsignarValoresComprobante(version, sello, serie, folio, fecha, formaDePago, noCertificado, certificado,
                subTotal, descuento, total, metodoDePago, tipodeComprobante, moneda);
        }
        private void AsignarValoresComprobante(string version, string sello, string serie, string folio, DateTime fecha,
           string formaDePago, string noCertificado, string certificado, decimal subTotal, decimal descuento, decimal total,
           string metodoDePago, string tipodeComprobante, string moneda)
        {


            /////////////////////////////////////////
            //////     Columna Atributos        /////
            /////////////////////////////////////////

            dgvComprobante.Rows.Add("Versión");
            dgvComprobante.Rows.Add("Serie");
            dgvComprobante.Rows.Add("Folio");
            dgvComprobante.Rows.Add("Fecha");
            dgvComprobante.Rows.Add("Sello");
            dgvComprobante.Rows.Add("Forma de pago");
            dgvComprobante.Rows.Add("No Certificado");
            dgvComprobante.Rows.Add("Certificado");
            dgvComprobante.Rows.Add("Subtotal");
            dgvComprobante.Rows.Add("Descuento");
            dgvComprobante.Rows.Add("Total");
            dgvComprobante.Rows.Add("Metodo de Pago");
            dgvComprobante.Rows.Add("Tipo de Comprobante");
            dgvComprobante.Rows.Add("Moneda");

            /////////////////////////////////////////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            dgvComprobante.Rows[0].Cells[1].Value = version;
            dgvComprobante.Rows[1].Cells[1].Value = serie;
            dgvComprobante.Rows[2].Cells[1].Value = folio;
            dgvComprobante.Rows[3].Cells[1].Value = fecha;
            dgvComprobante.Rows[4].Cells[1].Value = sello;
            dgvComprobante.Rows[5].Cells[1].Value = formaDePago;
            dgvComprobante.Rows[6].Cells[1].Value = noCertificado;
            dgvComprobante.Rows[7].Cells[1].Value = certificado;
            dgvComprobante.Rows[8].Cells[1].Value = subTotal;
            dgvComprobante.Rows[9].Cells[1].Value = descuento;
            dgvComprobante.Rows[10].Cells[1].Value = total;
            dgvComprobante.Rows[11].Cells[1].Value = metodoDePago;
            dgvComprobante.Rows[12].Cells[1].Value = tipodeComprobante;
            dgvComprobante.Rows[13].Cells[1].Value = moneda;
            txtFechaComprobante.Text = fecha.ToString();
        }


        private void ExtraeDatosEmisor()
        {
            /////////////////////////////////////////
            //////          Datos Emisor        /////
            /////////////////////////////////////////

            string rfcEmisor;
            string razonSocialEmisor;
            string regimenEmisor;

            /////////////////////////////////////////
            //////     Domicilio Fiscal         /////
            /////////////////////////////////////////

            //string calleDomFis;
            //string noExteriorDomFis;
            //string coloniaDomFis;
            //string localidadDomFis;
            //string municipioDomFis;
            //string estadoDomFis;
            //string paisDomFis;
            //string codigoPostalDomFis;


            /////////////////////////////////////////
            //////     Expedido en              /////
            /////////////////////////////////////////

            //string calleExEn;
            //string noExteriorExEn;
            //string coloniaExEn;
            //string localidadExEn;
            //string municipioExEn;
            //string estadoDomExEn;
            //string paisDomExEn;
            //string codigoPostalExEn;


            rfcEmisor = facturaVo.DatosFactura.Emisor.Rfc;
            razonSocialEmisor = facturaVo.DatosFactura.Emisor.Nombre;
            regimenEmisor = facturaVo.DatosFactura.Emisor.RegimenFiscal.ToString();

            //calleDomFis = facturaVo.DatosFactura.Emisor. DomicilioFiscal.calle;
            //noExteriorDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.noExterior;
            //coloniaDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.colonia;
            //localidadDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.localidad;
            //municipioDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.municipio;
            //estadoDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.estado;
            //paisDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.pais;
            //codigoPostalDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.codigoPostal;



            //calleExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.calle;
            //noExteriorExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.noExterior;
            //coloniaExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.colonia;
            //localidadExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.localidad;
            //municipioExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.municipio;
            //estadoDomExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.estado;
            //paisDomExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.pais;
            //codigoPostalExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.codigoPostal;

            AsignarValoresDatosEmisor(
                //paisDomFis, estadoDomFis, municipioDomFis,
                //coloniaDomFis, localidadDomFis, calleDomFis, noExteriorDomFis, codigoPostalDomFis,
                //calleExEn, noExteriorExEn, coloniaExEn, localidadExEn, municipioExEn, estadoDomExEn,
                //paisDomExEn, codigoPostalExEn, 
                rfcEmisor, razonSocialEmisor, regimenEmisor);


        }
        private void AsignarValoresDatosEmisor(string rfcEmisor, string razonSocialEmisor, string regimenEmisor)
        {

            /////////////////////////////////////////
            //////     Set caja de Texto        /////
            /////////////////////////////////////////

            this.txtRfcEmisor.Text = rfcEmisor;
            this.txtRfcRazonSocial.Text = razonSocialEmisor;
            this.txtRegimenEmisor.Text = regimenEmisor;
            this.txtRfcRazonSocial.Text = razonSocialEmisor;

            ////////////////////////////////////////////
            //////        Domicilio Fiscal        //////
            /////             Atributo            //////
            ////////////////////////////////////////////

            dvgDomFiscal.Rows.Add("Pais");
            dvgDomFiscal.Rows.Add("Estado");
            dvgDomFiscal.Rows.Add("Municipio");
            dvgDomFiscal.Rows.Add("Colonia");
            dvgDomFiscal.Rows.Add("Localidad");
            dvgDomFiscal.Rows.Add("Calle");
            dvgDomFiscal.Rows.Add("Num. Exterior");
            dvgDomFiscal.Rows.Add("Codigo Postal");


            //////////////////////////////////////////
            //////        Domicilio Fiscal       /////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            //dvgDomFiscal.Rows[0].Cells[1].Value = paisDomFis;
            //dvgDomFiscal.Rows[1].Cells[1].Value = estadoDomFis;
            //dvgDomFiscal.Rows[2].Cells[1].Value = municipioDomFis;
            //dvgDomFiscal.Rows[3].Cells[1].Value = coloniaDomFis;
            //dvgDomFiscal.Rows[4].Cells[1].Value = localidadDomFis;
            //dvgDomFiscal.Rows[5].Cells[1].Value = calleDomFis;
            //dvgDomFiscal.Rows[6].Cells[1].Value = noExteriorDomFis;
            //dvgDomFiscal.Rows[7].Cells[1].Value = codigoPostalDomFis;


            ////////////////////////////////////////////
            //////         Expedido En            //////
            /////            Atributo             //////
            ////////////////////////////////////////////

            dvgExpEn.Rows.Add("Pais");
            dvgExpEn.Rows.Add("Estado");
            dvgExpEn.Rows.Add("Municipio");
            dvgExpEn.Rows.Add("Colonia");
            dvgExpEn.Rows.Add("Localidad");
            dvgExpEn.Rows.Add("Calle");
            dvgExpEn.Rows.Add("Num. Exterior");
            dvgExpEn.Rows.Add("Codigo Postal");


            //////////////////////////////////////////
            //////        Expedido En           /////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            //dvgExpEn.Rows[0].Cells[1].Value = paisDomExEn;
            //dvgExpEn.Rows[1].Cells[1].Value = estadoDomExEn;
            //dvgExpEn.Rows[2].Cells[1].Value = municipioExEn;
            //dvgExpEn.Rows[3].Cells[1].Value = coloniaExEn;
            //dvgExpEn.Rows[4].Cells[1].Value = localidadExEn;
            //dvgExpEn.Rows[5].Cells[1].Value = calleExEn;
            //dvgExpEn.Rows[6].Cells[1].Value = noExteriorExEn;
            //dvgExpEn.Rows[7].Cells[1].Value = codigoPostalExEn;


        }

        private void ExtraeConceptos()
        {
            int cantidadConseptos = facturaVo.DatosFactura.Conceptos.Length;
            string[] noIdentificacion = new string[cantidadConseptos];
            string[] descripcion = new string[cantidadConseptos];
            decimal[] cantidad = new decimal[cantidadConseptos];
            string[] unidad = new string[cantidadConseptos];
            decimal[] valorUnitario = new decimal[cantidadConseptos]; ;
            decimal[] importe = new decimal[cantidadConseptos];

            decimal totalImpuestosRetenidos;
            decimal totalImpuestosTrasladados;

            int i;

            for (i = 0; i < cantidadConseptos; i++)
            {
                noIdentificacion[i] = facturaVo.DatosFactura.Conceptos[i].NoIdentificacion;
                descripcion[i] = facturaVo.DatosFactura.Conceptos[i].Descripcion;
                cantidad[i] = facturaVo.DatosFactura.Conceptos[i].Cantidad;
                unidad[i] = facturaVo.DatosFactura.Conceptos[i].Unidad;
                valorUnitario[i] = facturaVo.DatosFactura.Conceptos[i].ValorUnitario;
                importe[i] = facturaVo.DatosFactura.Conceptos[i].Importe;


            }
            AsignarValoresConseptos(cantidadConseptos, noIdentificacion, descripcion, cantidad, unidad, valorUnitario, importe);

            //CargarCodigoContable();

            /////////////////////////////////////////
            //////      Estrae Inpuestos        /////
            /////////////////////////////////////////

            //totalImpuestosRetenidos = facturaVo.DatosFactura.Impuestos.TotalImpuestosRetenidos;
            //totalImpuestosTrasladados = facturaVo.DatosFactura.Impuestos.TotalImpuestosTrasladados;

            //AsignaInpuestos(totalImpuestosRetenidos, totalImpuestosTrasladados);

        }

        private void AsignarValoresConseptos(int cantidadConseptos, string[] noIdentificacion, string[] descripcion,
           decimal[] cantidad, string[] unidad, decimal[] valorUnitario, decimal[] importe)
        {

            DataTable dt = new DataTable();
            int i;
            for (i = 0; i < cantidadConseptos; i++)
            {
                dgwConseptos.Rows.Add();
                dgwConseptos.Rows[i].Cells[0].Value = noIdentificacion[i];
                dgwConseptos.Rows[i].Cells[1].Value = descripcion[i];
                dgwConseptos.Rows[i].Cells[2].Value = cantidad[i];
                dgwConseptos.Rows[i].Cells[3].Value = unidad[i];
                dgwConseptos.Rows[i].Cells[4].Value = valorUnitario[i];
                dgwConseptos.Rows[i].Cells[5].Value = importe[i];

            }
            //dt = CargarCodigoContables();
            //cmbCodigocontables.DisplayMember = "MAYOR";
            //cmbCodigocontables.ValueMember = "CTA";
            //cmbCodigocontables.DataSource = dt ;
            //cmbCodigosCont.DisplayMember = "MAYOR";
            //cmbCodigosCont.ValueMember = "CTA";
            //cmbCodigosCont.DataSource = dt;

        }
        public void CargarMateriales()
        {
            string query = string.Empty;
            DataTable ds = new DataTable();
            DataTable dt = new DataTable();
            query = "SELECT codMat, descripcionMat FROM catalogomateriales;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            //conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds);
            conectar.Close();
            dt.Columns.Add("ids");
            dt.Columns.Add("cods");

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                DataRow row = dt.NewRow();

                row["ids"] = i;
                row["cods"] = ds.Rows[i].ItemArray[0].ToString() + "-" + ds.Rows[i].ItemArray[1].ToString();
                dt.Rows.Add(row);

            }
            cmbCodifoInterno.ValueMember = "ids";
            cmbCodifoInterno.DisplayMember = "cods";
            cmbCodifoInterno.DataSource = dt;

        }
        public void CargarCodigoContables()
        {
            string query = string.Empty;
            DataTable ds = new DataTable();
            DataTable dt = new DataTable();
            query = "SELECT MAYOR,CTA,SCTA,DESCRIPCION  FROM CATCONTABILIDAD;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            //conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds);
            conectar.Close();
            dt.Columns.Add("id");
            dt.Columns.Add("cod");

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                DataRow row = dt.NewRow();

                row["id"] = i;
                row["cod"] = ds.Rows[i].ItemArray[0].ToString() + "-" + ds.Rows[i].ItemArray[1].ToString() + "-" + ds.Rows[i].ItemArray[2].ToString() + "-" + ds.Rows[i].ItemArray[3].ToString();
                dt.Rows.Add(row);

            }
            cmbCont.ValueMember = "id";
            cmbCont.DisplayMember = "cod";
            cmbCont.DataSource = dt;

        }
        private void frmFacRecibidas_Load(object sender, EventArgs e)
        {
            //ConexionBD();
            CargarCodigoContable();
            CargarCodigoContables();
            CargarMateriales();
        }

        private void ConexionBD()
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }

        private void CargarCodigoContable()
        {
            string query = string.Empty;
            DataSet ds = new DataSet();

            query = "SELECT MAYOR,CTA,SCTA,DESCRIPCION  FROM CATCONTABILIDAD;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds, "MAYOR");

            foreach (DataRow row in ds.Tables["MAYOR"].Rows)
            {
                cmbCodigosCont.Items.Add(row.ItemArray[0] + "-" + row.ItemArray[1] + "-" + row.ItemArray[2] + "-" + row.ItemArray[3]);
                //dgwConseptos.Rows.Add(cmbCodigoContable.Items.Add(row.ItemArray[0] + "-" + row.ItemArray[1] + "-" + row.ItemArray[2] + "-" + row.ItemArray[3]));

            }
            txtUsoCFDI.Text = strUUID;
            txtRazonSocial.Text = facturaVo.DatosFactura.Emisor.Nombre;
            txtFolioFiscal.Text = facturaVo.DatosFactura.Folio;

            ////dataGridView1 = new
            //dgwConseptos.Columns[6].Name = "Codigo Contable";

            //DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
            //cmb.HeaderText = "Select Data";
            //cmb.Name = "cmb";
            ////cmb.MaxDropDownItems = 4;
            //cmb.Items.Add("True");
            //cmb.Items.Add("False");
            //dgwConseptos.Columns.Add(cmb);




        }

        private void toolGuardar_Click(object sender, EventArgs e)
        {
            Validar();
        }

        private void Validar()
        {
            if (txtPoliza.Text != "" && dtFecha.Text != "")
            {
                GuardarFacCodigo();
            }
            else
            {
                MessageBox.Show("DEDE INGRESAR TODOS LOS DATOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GuardarFacCodigo()
        {
            string query = string.Empty;
            string strMayor = string.Empty;
            string strPoliza = string.Empty;
            string strCTA = string.Empty;
            string strSCTA = string.Empty;
            string strCodigo = string.Empty;
            string strUUID2 = string.Empty;
            string strTipo = string.Empty;
            string strFecha = string.Empty;
            string strIdentificador = string.Empty;
            string strId = string.Empty;
            strPoliza = txtPoliza.Text;
            strFecha = dtFecha.Text;
            strCodigo = cmbCodigosCont.Text;
            if (strCodigo != "")
            {

                String[] substrings = strCodigo.Split('-');
                String[] substring = strUUID.Split('.');
                strUUID2 = substring[0];



                if (substrings.Length > 0)
                    strMayor = substrings[0];
                if (substrings.Length > 0)
                    strCTA = substrings[1];
                if (substrings.Length > 0)
                    strSCTA = substrings[2];
                int intNum = BuscarConcepto(strUUID2);


                if (intNum == 0)
                {
                    //for (int i = 0; i < dgwConseptos.Rows.Count; i++)
                    //{
                    //    if (dgwConseptos.Rows[i].Cells[7].Value != null)
                    //        strId = dgwConseptos.Rows[i].Cells[7].Value.ToString();


                    //}

                    query = "INSERT INTO FACTURASCODIGOS VALUE(" + "'" + strMayor + "','" + strCTA + "','" + strSCTA + "','" + strPoliza + "','" + strUUID2 + "','" + strFecha + "','R');";
                    query = query + " UPDATE FACEMITIDAS SET CODCONT = " + strPoliza + " WHERE UUID = '" + strUUID + "';";
                    conectar.Open();
                    MySqlCommand mycomand = new MySqlCommand(query, conectar);
                    mycomand.ExecuteReader();
                    conectar.Close();


                    MessageBox.Show("DATOS GUARDADOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {


                for (int i = 0; i < dgwConseptos.Rows.Count; i++)
                {



                    //int intIndex = cmbCont.Index;
                    strCodigo = sValue;
                    String[] substrings = strCodigo.Split('-');
                    String[] substring = strUUID.Split('.');
                    strUUID2 = substring[0];
                    if (dgwConseptos.Rows[i].Cells[0].Value != null)
                        strIdentificador = dgwConseptos.Rows[i].Cells[0].Value.ToString();
                    // string.IsNullOrEmpty(dgwConseptos.Rows[i].Cells[0] ? "" : substrings[2];
                    if (dgwConseptos.Rows[i].Cells[7].Value != null)
                        strId = dgwConseptos.Rows[i].Cells[7].Value.ToString();

                    if (substrings.Length > 0)
                        strMayor = substrings[0];
                    if (substrings.Length > 0 && substrings[1] != "")
                        strCTA = substrings[1];
                    if (substrings.Length > 0)
                        strSCTA = substrings[2];
                    int intNum = BuscarConceptos(strUUID2, strIdentificador);
                    if (intNum == 0)
                    {

                        query = "INSERT INTO CONCEPTOSGENERALES VALUE(" + "'" + strUUID2 + "','" + strIdentificador + "','" + strMayor + "','" + strCTA + "','" + strSCTA + "','" + strFecha + "','E'"+",'" +strId+"');";
                        query = query + " UPDATE FACEMITIDAS SET CODCONT = " + strPoliza + " WHERE UUID = '" + strUUID + "';";
                        conectar.Open();
                        MySqlCommand mycomand = new MySqlCommand(query, conectar);
                        mycomand.ExecuteReader();
                        conectar.Close();

                    }
                }
                MessageBox.Show("DATOS GUARDADOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }
        private int BuscarConceptos(string strUUID2, string strIdentificador)
        {
            string query = string.Empty;
            int intNum = 0;
            DataTable ds = new DataTable();
            query = "SELECT *  FROM CONCEPTOSGENERALES WHERE CFDI = '" + strUUID2 + "' AND NO_IDENTIFICADOR ='" + strIdentificador + "' AND TIPO = 'E';";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            conectar.Close();

            myAdapter.Fill(ds);

            if (ds.Rows.Count > 0)
            {
                intNum = ds.Rows.Count;
            }
            return intNum;


        }
        private int BuscarConcepto(string strUUID2)
        {
            string query = string.Empty;
            int intNum = 0;
            DataTable ds = new DataTable();
            query = "SELECT *  FROM FACTURASCODIGOS WHERE UUID = '" + strUUID2 + "';";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds);
            conectar.Close();
            if (ds.Rows.Count > 0)
            {
                intNum = ds.Rows.Count;
            }
            return intNum;
        }

        private void dgwConseptos_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgwConseptos.CurrentRow != null)
            {
                DataGridViewRow dr = dgwConseptos.CurrentRow;
                Convert.ToInt32(dr.Cells["cmbCont"].Value == DBNull.Value ? "0" : dr.Cells["cmbCont"].Value);
                Convert.ToInt32(dr.Cells["cmbCodifoInterno"].Value == DBNull.Value ? "0" : dr.Cells["cmbCodifoInterno"].Value);
            }
        }
        private void dgwConseptos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgwConseptos.CurrentCell.ColumnIndex == 6 && e.Control is ComboBox)
            {
                selectedIndexs = 6;
                ComboBox comboBox = e.Control as ComboBox;
                if (comboBox != null)
                {
                    comboBox.SelectedIndexChanged -= new
                         EventHandler(comboBox_SelectedChanged);

                    comboBox.SelectedIndexChanged += new
                         EventHandler(comboBox_SelectedChanged);
                }
            }
            if (dgwConseptos.CurrentCell.ColumnIndex == 7 && e.Control is ComboBox)
            {
                selectedIndexs = 7;
                ComboBox comboBox = e.Control as ComboBox;
                if (comboBox != null)
                {
                    comboBox.SelectedIndexChanged -= new
                         EventHandler(comboBox1_SelectedChanged);

                    comboBox.SelectedIndexChanged += new
                         EventHandler(comboBox1_SelectedChanged);
                }
            }
        }
        void comboBox_SelectedChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            int selectedIndex = comboBox.SelectedIndex;
            Object selectedItem = comboBox.SelectedItem;

            DataRowView oDataRowView = comboBox.SelectedItem as DataRowView;

            if (oDataRowView != null)
            {
                try
                {
                    if (selectedIndexs == 6)

                        sValue = oDataRowView.Row["cod"] as string;
                    if (selectedIndexs == 7)
                        sValue = oDataRowView.Row["cods"] as string;
                }
                catch (Exception ex) { }
            }
        }
        void comboBox1_SelectedChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            int selectedIndex = comboBox.SelectedIndex;
            Object selectedItem = comboBox.SelectedItem;

            DataRowView oDataRowView = comboBox.SelectedItem as DataRowView;

            if (oDataRowView != null)
            {
                sValueMat = oDataRowView.Row["cods"] as string;
            }
        }
        private void frmFacRecibidas_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogo = MessageBox.Show("¿Desea cerrar el programa?",
             "Cerrar el programa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogo == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;

                frmRecibidas frmR = new frmRecibidas(strAnioGeneral, strMesGeneral, "ok");
                frmR.Hide();
            }
        }
    }
}
