﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Facturacion.JARC
{
    public partial class frmRecibidas : Form
    {
        MySqlConnection conectar;
        Factura facturaVo = new Factura();
        private string srrUUID;
        string strAnioGeneral = string.Empty;
        string strMesGeneral = string.Empty;
        string strVals = string.Empty;
        public frmRecibidas(string strAnioGeneral1, string strMesGeneral1,string strVal)
        {
            strAnioGeneral = strAnioGeneral1;
            strMesGeneral = strMesGeneral1;
            InitializeComponent();
            conectar = ConnectionString.ObtenerConexion();
            if (strAnioGeneral != "" && strMesGeneral != "")
            {
               //toolStripLimpiar_Click(null,null);
               // InitializeComponent();
                //toolStripBuscar_Click(null, null);
                //BuscarPoliza();
            }
        }

        public void toolStripBuscar_Click(object sender, EventArgs e)
        {
            ValidarCampos();
            AgregarConseutivo();
            //BuscarPoliza();
        }
        private void CargarCombo()
        {
            string mes = string.Empty;
            DataSet ds = new DataSet();
            string query = string.Empty;

            query = "SELECT *  FROM MESES;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds, "MES");

            foreach (DataRow row in ds.Tables["MES"].Rows)
            {
                cmbMes.Items.Add(row.ItemArray[0] + "-" + row.ItemArray[1]);
            }

        }
        private void ValidarCampos()
        {
            if(strMesGeneral != "")
            {
                BuscarDatos();
            }else if (cmbMes == null || cmbAnio == null)
            {

                //dgbRecibidos.DataSource = null;
                MessageBox.Show("DEBE INGRESAR EL MES Y EL AÑO", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (cmbMes.Text == "" || cmbAnio.Text == "")
            {
                MessageBox.Show("DEBE INGRESAR EL MES Y EL AÑO", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                BuscarDatos();
            }
        }

        private void BuscarDatos()
        {
            BuscarFacturasRecibidas();
            AgregarConseutivo();
            SumarTotales();
            BuscarPoliza();
        }

        private void AgregarConseutivo()
        {
            int intContador = 1;
            for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            {
                if (i == 0)
                {
                    dgbRecibidos.Rows[i].Cells[0].Value = "1";
                    intContador++;
                }
                else if (dgbRecibidos.Rows[i].Cells[2].Value == null)
                {
                    // intContador = i - 1;
                    dgbRecibidos.Rows[i].Cells[0].Value = "";

                }
                else
                {
                    //  intContador = i;
                    dgbRecibidos.Rows[i].Cells[0].Value = intContador;
                    intContador++;

                }
                //else if (dgbEmitidos.Rows[i].Cells[2].Value == null)
                //{
                //    dgbEmitidos.Rows[i].Cells[0].Value = "";
                //}
            }
        }

        private void BuscarFacturasRecibidas()
        {
            int i = 0;
            string strMes = string.Empty;
            string strAno = string.Empty;
            string strTest = string.Empty;
            string strFolio = string.Empty;
            string strFecha = string.Empty;
            string strHora = string.Empty;
            string strDia = string.Empty;
            string strMeses = string.Empty;
            string strAnoS = string.Empty;
            string strImpuestos = string.Empty;
            string strNombre = string.Empty;
            List<string> listUUID = new List<string>();


            if (strAnioGeneral != "" && strMesGeneral != "")
            {
                strMes = strAnioGeneral.Substring(0, 2);
                strAno = strMesGeneral;
                strAno = strAno + "-" + strMes;
            }
            else
            {
                strMes = cmbMes.Text.ToString();
                strMes = strMes.Substring(0, 2);
                strAno = cmbAnio.Text.ToString();
                strAno = strAno + "-" + strMes;
            }
            // DirectoryInfo di = new DirectoryInfo(@"C:\\Users\\Jazmin\\OneDrive\\Documentos\\Facturas_Recibidas\\" + strAno );
            DirectoryInfo di = new DirectoryInfo(@"C:\\CapDigi\\CFDiDescargaXml\\Recibidos\\RACA490321B88\\" + strAno);
            foreach (var fi in di.GetFiles())
            {
                strTest = fi.Name;
                XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
                XmlTextReader reader = new XmlTextReader(@"" + di + "\\" + strTest + "");
                Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
                facturaVo.DatosFactura = factura;

                strFecha = facturaVo.DatosFactura.Fecha.ToString().Replace('/', '-');
                strAnoS = strFecha.Substring(6, 4);
                strMeses = strFecha.Substring(3, 2);
                strDia = strFecha.Substring(0, 2);
                strHora = strFecha.Substring(11, 8);

                if (strMes.Equals(strMeses))
                {
                    if(lblOk.Text != "ok")
                    dgbRecibidos.Rows.Add();
                    strFolio = facturaVo.DatosFactura.Folio;
                    strNombre = facturaVo.DatosFactura.Receptor.Nombre;
                    if (facturaVo.DatosFactura.Impuestos != null)
                    {
                        strImpuestos = facturaVo.DatosFactura.Impuestos.TotalImpuestosTrasladados.ToString();
                    }
                    else
                    {
                        strImpuestos = "";
                    }

                    if (strFolio == null)
                        strFolio = "";
                    if (strNombre == null)
                        strNombre = "";
                    if (strImpuestos == null)
                        strImpuestos = "";

                    dgbRecibidos.Rows[i].Cells[1].Value = strFolio;
                    dgbRecibidos.Rows[i].Cells[2].Value = strAno + "-" + strMeses + "-" + strDia + " " + strHora;
                    dgbRecibidos.Rows[i].Cells[3].Value = facturaVo.DatosFactura.Receptor.Rfc.ToString();
                    dgbRecibidos.Rows[i].Cells[4].Value = strNombre;
                    dgbRecibidos.Rows[i].Cells[5].Value = facturaVo.DatosFactura.SubTotal.ToString();
                    dgbRecibidos.Rows[i].Cells[6].Value = strImpuestos;
                    dgbRecibidos.Rows[i].Cells[7].Value = facturaVo.DatosFactura.Total.ToString();
                    dgbRecibidos.Rows[i].Cells[17].Value = strTest;
                    i++;
                }
                else
                {
                    listUUID.Add(strTest);
                }
            }
            if (listUUID.Count > 0)
            {
                i++;
                dgbRecibidos.Rows.Add();
                foreach (string s in listUUID)
                {

                    XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
                    XmlTextReader reader = new XmlTextReader(@"" + di + "\\" + s + "");
                    Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
                    facturaVo.DatosFactura = factura;

                    strFecha = facturaVo.DatosFactura.Fecha.ToString().Replace('/', '-');
                    strAnoS = strFecha.Substring(6, 4);
                    strMeses = strFecha.Substring(3, 2);
                    strDia = strFecha.Substring(0, 2);
                    strHora = strFecha.Substring(11, 8);

                    dgbRecibidos.Rows.Add();
                    strFolio = facturaVo.DatosFactura.Folio;
                    strNombre = facturaVo.DatosFactura.Receptor.Nombre;
                    if (facturaVo.DatosFactura.Impuestos != null)
                    {
                        strImpuestos = facturaVo.DatosFactura.Impuestos.TotalImpuestosTrasladados.ToString();
                    }
                    else
                    {
                        strImpuestos = "";
                    }
                    if (strFolio == null)
                        strFolio = "";
                    if (strNombre == null)
                        strNombre = "";
                    if (strImpuestos == null)
                        strImpuestos = "";

                    dgbRecibidos.Rows[i].Cells[1].Value = strFolio;
                    dgbRecibidos.Rows[i].Cells[2].Value = strAno + "-" + strMeses + "-" + strDia + " " + strHora;
                    dgbRecibidos.Rows[i].Cells[3].Value = facturaVo.DatosFactura.Receptor.Rfc.ToString();
                    dgbRecibidos.Rows[i].Cells[4].Value = strNombre;
                    dgbRecibidos.Rows[i].Cells[5].Value = facturaVo.DatosFactura.SubTotal.ToString();
                    dgbRecibidos.Rows[i].Cells[6].Value = strImpuestos;
                    dgbRecibidos.Rows[i].Cells[7].Value = facturaVo.DatosFactura.Total.ToString();
                    dgbRecibidos.Rows[i].Cells[17].Value = strTest;
                    i++;
                }
            }
            lblTotal.Text = dgbRecibidos.Rows.Count.ToString();
        }

        private void frmRecibidas_Load(object sender, EventArgs e)
        {
            try
            {
               // conectar = ConnectionString.ObtenerConexion();
                CargarCombo();
                CargarAnio();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }

        private void CargarAnio()
        {
            DateTime anio = DateTime.Today;

            for (int i = anio.Year; i >= 1900; i--)
            {
                cmbAnio.Items.Add(i);
            }

        }
        private void dgbEmitidos_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            string strAno = string.Empty;
            string strMes = string.Empty;
            string strFecha = string.Empty;
            string strUUID = string.Empty;
            string strRuta = string.Empty;
            string strPoliza = string.Empty;
            string strConsecutivo = string.Empty;
            int i = dgbRecibidos.CurrentCell.RowIndex;
            int j = dgbRecibidos.CurrentCell.ColumnIndex;
            if (dgbRecibidos.Rows[i].Cells[17].Value != null)
            {
                strUUID = dgbRecibidos.Rows[i].Cells[17].Value.ToString();
                strFecha = dgbRecibidos.Rows[i].Cells[2].Value.ToString();
                strAno = strFecha.Substring(0, 4);
                strMes = strFecha.Substring(5, 2);
                strRuta = strAno + "-" + strMes + "\\" + strUUID;
                if (dgbRecibidos.Rows[i].Cells[15].Value != null)
                    strPoliza = dgbRecibidos.Rows[i].Cells[15].Value.ToString();
                strConsecutivo = dgbRecibidos.Rows[i].Cells[0].Value.ToString();

                frmFacRecibidas frmFeM = new frmFacRecibidas(strRuta, strUUID, strConsecutivo, cmbMes.Text, cmbAnio.Text);
                DialogResult result = frmFeM.ShowDialog();
                if (result == DialogResult.OK) // Test result.
                {
                }
                else
                {
                    //toolStripLimpiar_Click(null, null);
                    lblOk.Text = "ok";
                    toolStripBuscar_Click(null, null);
                }
            }
        }

        private void toolStripLimpiar_Click(object sender, EventArgs e)
        {
            dgbRecibidos.DataSource = null;
            dgbRecibidos.Rows.Clear();
            cmbMes.SelectedIndex = -1;
            cmbAnio.SelectedIndex = -1;
        }

        private void toolStripGuardar_Click(object sender, EventArgs e)
        {
            if (dgbRecibidos.Rows.Count > 0)
            {
                GuardarRecibidos();
                GuardarTotalMensual();

            }
            else
            {
                MessageBox.Show("DEDE REALIZAR UNA BUSQUEDA", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void GuardarRecibidos()
        {
            string query = string.Empty;
            string querys = string.Empty;
            string strConsulta = string.Empty;
            string srrUUID = string.Empty;
            string strNombre = string.Empty;
            string strIVA = string.Empty;
            string strConsecutivo = string.Empty;

            for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            {
                if (dgbRecibidos.Rows[i].Cells[2].Value != null)
                {

                    if (dgbRecibidos.Rows[i].Cells[6].Value != null)
                        strConsecutivo = dgbRecibidos.Rows[i].Cells[0].Value.ToString();

                    strNombre = dgbRecibidos.Rows[i].Cells[4].Value.ToString();
                    strIVA = dgbRecibidos.Rows[i].Cells[6].Value.ToString();
                    if (strNombre == null)
                        strNombre = "";

                    if (strIVA == null || strIVA == "")
                        strIVA = "0";

                    strConsulta = "SELECT UUID  FROM facrecibidas WHERE UUID ='" + dgbRecibidos.Rows[i].Cells[17].Value.ToString() + "';";

                    MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
                    MySqlDataReader myreader = mycomands.ExecuteReader();

                    if (myreader.Read())
                        srrUUID = myreader["UUID"].ToString();

                    if (srrUUID == "")
                    {
                        querys = "INSERT INTO facrecibidas " + "VALUE(" + strConsecutivo + ",'" + dgbRecibidos.Rows[i].Cells[1].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[2].Value.ToString().Substring(0, 10) + "','" + dgbRecibidos.Rows[i].Cells[3].Value.ToString() + "','" + strNombre + "'," + dgbRecibidos.Rows[i].Cells[5].Value.ToString() + "," + strIVA + "," + dgbRecibidos.Rows[i].Cells[7].Value.ToString() + ",'" + dgbRecibidos.Rows[i].Cells[17].Value.ToString() + "'" + ",''" + ");";
                        query = querys + query;
                    }
                    myreader.Close();
                }

            }
            if (query != "")
            {
                MySqlCommand mycomand = new MySqlCommand(query, conectar);
                MySqlDataReader myreader = mycomand.ExecuteReader();
                myreader.Close();
            }
            //string query = string.Empty;
            //string querys = string.Empty;
            //string strConsulta = string.Empty;
            //string srrUUID = string.Empty;
            //string strNombre = string.Empty;
            //string strIVA = string.Empty;
            //string strConsecutivo = string.Empty;

            //for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            //{
            //    if (dgbRecibidos.Rows[i].Cells[2].Value != null)
            //    {

            //        if (dgbRecibidos.Rows[i].Cells[6].Value != null)
            //            strConsecutivo = dgbRecibidos.Rows[i].Cells[0].Value.ToString();

            //        strNombre = dgbRecibidos.Rows[i].Cells[4].Value.ToString();
            //        strIVA = dgbRecibidos.Rows[i].Cells[6].Value.ToString();
            //        if (strNombre == null)
            //            strNombre = "";

            //        if (strIVA == null || strIVA == "")
            //            strIVA = "0";



            //        strConsulta = "SELECT UUID  FROM FACRECIBIDAS WHERE UUID ='" + dgbRecibidos.Rows[i].Cells[17].Value.ToString() + "';";

            //        MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
            //        MySqlDataReader myreader = mycomands.ExecuteReader();

            //        if (myreader.Read())
            //            srrUUID = myreader["UUID"].ToString();

            //        if (srrUUID == "")
            //        {
            //            querys = "INSERT INTO FACRECIBIDAS " + "VALUE(" + strConsecutivo + ",'" + dgbRecibidos.Rows[i].Cells[1].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[2].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[3].Value.ToString() + "','" + strNombre + "'," + dgbRecibidos.Rows[i].Cells[5].Value.ToString() + "," + strIVA + "," + dgbRecibidos.Rows[i].Cells[7].Value.ToString() + ",'" + dgbRecibidos.Rows[i].Cells[17].Value.ToString() + "'" + ",''" + ");";

            //            query = querys + query;
            //        }
            //        myreader.Close();
            //    }

            //}
            //if (query != "")
            //{
            //    //MySqlCommand mycomand = new MySqlCommand(query, conectar);
            //    //mycomand.ExecuteReader();

            //    MySqlCommand mycomand = new MySqlCommand(query, conectar);
            //    MySqlDataReader myreader = mycomand.ExecuteReader();
            //    myreader.Close();
            // //   MessageBox.Show("DATOS GUARDADOS", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);



            //    //  conectar.Close();

            //    //  MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //else
            //{

            //    MessageBox.Show("LAS FACTURAS YA SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

            //string query = string.Empty;
            //string querys = string.Empty;
            //string strConsulta = string.Empty;
            //string srrUUID = string.Empty;

            //for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            //{
            //    if (dgbRecibidos.Rows[i].Cells[2].Value != null)
            //    {
            //        strConsulta = "SELECT UUID  FROM FACRECIBIDAS WHERE UUID ='" + dgbRecibidos.Rows[i].Cells[17].Value.ToString() + "';";

            //        MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
            //        MySqlDataReader myreader = mycomands.ExecuteReader();

            //        if (myreader.Read())
            //            srrUUID = myreader["UUID"].ToString();
            //        if (srrUUID == "")
            //        {
            //            querys = "INSERT INTO FACRECIBIDAS " + "VALUE(" + dgbRecibidos.Rows[i].Cells[0].Value.ToString() + ",'" + dgbRecibidos.Rows[i].Cells[1].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[2].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[3].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[4].Value.ToString() + "'," + dgbRecibidos.Rows[i].Cells[5].Value.ToString() + "," + dgbRecibidos.Rows[i].Cells[6].Value.ToString() + "," + dgbRecibidos.Rows[i].Cells[7].Value.ToString() + ",'" + dgbRecibidos.Rows[i].Cells[17].Value.ToString() + "'" + ");";
            //            query = querys + query;
            //        }
            //        myreader.Close();
            //    }
            //    if (query != "")
            //    {
            //        MySqlCommand mycomand = new MySqlCommand(query, conectar);
            //        mycomand.ExecuteReader();
            //        MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //    else
            //    {
            //        GuardarTotalMensual();
            //        MessageBox.Show("LAS FACTURAS YA SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}
        }

        private void SumarTotales()
        {
            double dSubtotal = 0;
            double dIVA = 0;
            double dTotal = 0;
            double dSumaSubtotal = 0;
            double dSumaIVA = 0;
            double dSumaTotal = 0;
            string strTotal = string.Empty;
            string strIVA = string.Empty;
            string strSubtotal = string.Empty;
            int intCont = 0;
            for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            {
                intCont = i;
                // strTotal = dgbEmitidos.Rows[i].Cells[5].Value.ToString();
                // if (strTotal == null || strTotal == "")
                if (dgbRecibidos.Rows[i].Cells[5].Value == null || dgbRecibidos.Rows[i].Cells[5].Value.ToString() == "")
                {
                    strTotal = "0";
                }
                else
                {
                    strTotal = dgbRecibidos.Rows[i].Cells[5].Value.ToString();
                }
                // strIVA = dgbEmitidos.Rows[i].Cells[6].Value.ToString();
                // if (strIVA == null || strIVA == "")
                if (dgbRecibidos.Rows[i].Cells[6].Value == null || dgbRecibidos.Rows[i].Cells[6].Value.ToString() == "")
                {
                    strIVA = "0";
                }
                else
                {
                    strIVA = dgbRecibidos.Rows[i].Cells[6].Value.ToString();
                }
                //  strSubtotal = dgbEmitidos.Rows[i].Cells[7].Value.ToString();
                // if (strSubtotal == null || strSubtotal == "")
                if (dgbRecibidos.Rows[i].Cells[7].Value == null || dgbRecibidos.Rows[i].Cells[7].Value.ToString() == "")
                {
                    strSubtotal = "0";
                }
                else
                {
                    strSubtotal = dgbRecibidos.Rows[i].Cells[7].Value.ToString();
                }

                dSubtotal = Convert.ToDouble(strTotal);
                dIVA = Convert.ToDouble(strIVA);
                dTotal = Convert.ToDouble(strSubtotal);

                dSumaSubtotal = dSumaSubtotal + dSubtotal;
                dSumaIVA = dSumaIVA + dIVA;
                dSumaTotal = dSumaTotal + dTotal;
                if (dgbRecibidos.Rows[i].Cells[2].Value == null)
                {
                    dgbRecibidos.Rows[i].Cells[12].Value = dSumaSubtotal;
                    dgbRecibidos.Rows[i].Cells[13].Value = dSumaIVA;
                    dgbRecibidos.Rows[i].Cells[14].Value = dSumaTotal;

                    return;
                }

            }

            dgbRecibidos.Rows[intCont].Cells[12].Value = dSumaSubtotal;
            dgbRecibidos.Rows[intCont].Cells[13].Value = dSumaIVA;
            dgbRecibidos.Rows[intCont].Cells[14].Value = dSumaTotal;
        }


        private void GuardarTotalMensual()
        {
            string querys = string.Empty;
            string strMes = string.Empty;
            string strAno = string.Empty;
            string strConsulta = string.Empty;
            string strCont = string.Empty;
            strMes = cmbMes.Text.Substring(0, 2);
            strAno = cmbAnio.Text;
            for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            {
                if (dgbRecibidos.Rows[i].Cells[12].Value != null)
                {

                    //ACOMODAR 
                    strConsulta = "SELECT ANO  FROM INGRESOSEGRESOSMENSUALES WHERE MES = " + strMes + " AND MENSUAL = 'R' AND ANO = " + strAno + "; ";

                    MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
                    MySqlDataReader myreader = mycomands.ExecuteReader();

                    if (myreader.Read())
                    // if (strCont != "")
                    {
                        strCont = myreader["ANO"].ToString();
                        if (strCont != "")
                            querys = "UPDATE INGRESOSEGRESOSMENSUALES SET IMPORTE = '" + dgbRecibidos.Rows[i].Cells[12].Value.ToString() + "',  IVA = '" + dgbRecibidos.Rows[i].Cells[13].Value.ToString() + "', TOTAL = '" + dgbRecibidos.Rows[i].Cells[14].Value.ToString() + "'  WHERE ANO =" + strAno + " AND MES = '" + strMes + "' AND MENSUAL = 'R'";
                    }
                    else
                    {
                        querys = "INSERT INTO INGRESOSEGRESOSMENSUALES " + "VALUE(" + cmbAnio.Text + ",'" + strMes + "','" + dgbRecibidos.Rows[i].Cells[12].Value.ToString() + "'," + "''" + ",'" + dgbRecibidos.Rows[i].Cells[13].Value.ToString() + "','" + dgbRecibidos.Rows[i].Cells[14].Value.ToString() + "','R'" + ");";
                    }
                    myreader.Close();

                    //MySqlCommand mycomand = new MySqlCommand(querys, conectar);
                    //mycomand.ExecuteReader();

                    MySqlCommand mycomand = new MySqlCommand(querys, conectar);
                    MySqlDataReader myreader1 = mycomand.ExecuteReader();
                    myreader1.Close();
                    MessageBox.Show("DATOS GUARDADOS", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);



                }
                //else
                //{
                //    return;
                //}
            }

        }

        private void BuscarPoliza()
        {
            string strConsulta = string.Empty;
            string strUUID = string.Empty;
            string strPoliza = string.Empty;
            int intI = 0;
            for (int i = 0; i < dgbRecibidos.Rows.Count; i++)
            {
                if (dgbRecibidos.Rows[i].Cells[17].Value != null)
                {
                    srrUUID = dgbRecibidos.Rows[i].Cells[17].Value.ToString();
                    //  srrUUID 
                    string[] words = srrUUID.Split('.');
                    srrUUID = words[0].ToString();
                    strConsulta = "SELECT NO_IDENTIFICADOR  FROM CONCEPTOSGENERALES WHERE CFDI ='" + srrUUID + "';";

                    MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
                    //conectar.Open();
                    MySqlDataReader myreader = mycomands.ExecuteReader();

                    intI = i;
                    if (myreader.Read())
                    {
                        strPoliza = myreader["NO_IDENTIFICADOR"].ToString();
                        dgbRecibidos.Rows[intI].Cells[15].Value = strPoliza;
                        if (strPoliza != "")
                            dgbRecibidos.Rows[i].DefaultCellStyle.BackColor = Color.Aquamarine;
                        myreader.Close();
                    }
                    else
                    {
                        myreader.Close();
                        srrUUID = dgbRecibidos.Rows[i].Cells[17].Value.ToString();

                        string[] words1 = srrUUID.Split('.');
                        srrUUID = words1[0].ToString();

                        strPoliza = BuscarPolis(srrUUID);
                        if (strPoliza != "")
                            dgbRecibidos.Rows[i].DefaultCellStyle.BackColor = Color.Aquamarine;


                    }

                }
            }
        }
        private string BuscarPolis(string strUUID2)
        {
            string query = string.Empty;
            string strPoliza = string.Empty;
            string strConsulta = String.Empty;
            int intI = 0;
            DataTable ds = new DataTable();
            strConsulta = "SELECT POLIZA  FROM FACTURASCODIGOS WHERE UUID ='" + strUUID2 + "';";
            MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
            MySqlDataReader myreader = mycomands.ExecuteReader();



            if (myreader.Read())
            {
                strPoliza = myreader["POLIZA"].ToString();
                dgbRecibidos.Rows[intI].Cells[15].Value = strPoliza;



            }
            myreader.Close();
            return strPoliza;
        }

    }
}
