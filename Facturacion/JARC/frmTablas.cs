﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Facturacion.JARC
{
    public partial class frmTablas : Form
    {
        string conn = string.Empty;
        MySqlConnection conectar;
        int i = 0;
        int j = 0;

        public frmTablas()
        {
            InitializeComponent();
        }



        private void frmTablas_Load(object sender, EventArgs e)
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
                InsertarFilas();
                CargarCombo();
                CargarAnio();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }

        }

        private void InsertarFilas()
        {
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
            dgvDatos.Rows.Add();
        }

        private void CargarAnio()
        {
            DateTime anio = DateTime.Today;

            for (int i = anio.Year; i >= 1900; i--)
            {
                cmbAnio.Items.Add(i);
            }

        }

        private void CargarCombo()
        {
            string mes = string.Empty;
            DataSet ds = new DataSet();
            string query = string.Empty;

            query = "SELECT *  FROM MESES;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds, "MES");

            foreach (DataRow row in ds.Tables["MES"].Rows)
            {
                cmbTablas.Items.Add(row.ItemArray[1]);
            }

        }

        private void toolStripBuscar_Click_1(object sender, EventArgs e)
        {
            ValidaCampos();

        }

        private void ValidaCampos()
        {
            if (cmbTablas.Text == "" || cmbAnio.Text == "")
            {
                MessageBox.Show("DEBE INGRESAR EL MES Y EL AÑO", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                BuscarTabla();
            }
        }

        private void BuscarTabla()
        {
            DataTable dt = new DataTable();
            string query = string.Empty;

            dgvDatos.DataSource = null;

            query = "SELECT LIM_INFERIOR,LIM_SUPERIOR,CUOTA_FIJA,PORCENTAJE  from tablas WHERE ANO = " + cmbAnio.Text + " AND MES = '" + cmbTablas.Text + "';";
            conectar.Close();
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgvDatos.Rows[i].Cells[0].Value = dt.Rows[i].ItemArray[0];
                    dgvDatos.Rows[i].Cells[1].Value = dt.Rows[i].ItemArray[1];
                    dgvDatos.Rows[i].Cells[2].Value = dt.Rows[i].ItemArray[2];
                    dgvDatos.Rows[i].Cells[3].Value = dt.Rows[i].ItemArray[3];
                }
            }
            else
            {
                MessageBox.Show("No hay datos");
            }
        }
        private void dgvDatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                DataObject d = dgvDatos.GetClipboardContent();
                Clipboard.SetDataObject(d);
                e.Handled = true;
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
                string s = Clipboard.GetText();
                string[] lines = s.Split('\n');
                int row = dgvDatos.CurrentCell.RowIndex;
                int col = dgvDatos.CurrentCell.ColumnIndex;
                foreach (string line in lines)
                {
                    if (row < dgvDatos.RowCount && line.Length >
0)
                    {
                        string[] cells = line.Split('\t');
                        for (int i = 0; i < cells.GetLength(0); ++i)
                        {
                            if (col + i < this.dgvDatos.ColumnCount)
                            {
                                dgvDatos[col + i, row].Value = Convert.ChangeType(cells[i], dgvDatos[col + i, row].ValueType);
                            }
                            else
                            {
                                break;
                            }
                        }
                        row++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private void toolStripLimpiar_Click(object sender, EventArgs e)
        {
            cmbAnio.SelectedIndex = -1;
            cmbTablas.SelectedIndex = -1;
            dgvDatos.Rows.Clear();
            InsertarFilas();
            txtCuotaFija.Clear();
            txtLimiteInferior.Clear();
            txtPorcentaje.Clear();
            txtLimiteSuperior.Clear();
        }

        private void toolStripGuardar_Click(object sender, EventArgs e)
        {
            string query = string.Empty;
            string querys = string.Empty;
            if (dgvDatos.Rows.Count > 0)
            {
                for (int i = 0; i < dgvDatos.Rows.Count; i++)
                {
                    query = "INSERT INTO TABLAS VALUE('" + cmbAnio.Text + "','" + cmbTablas.Text + "','" + dgvDatos.Rows[i].Cells[0].Value.ToString() + "','" + dgvDatos.Rows[i].Cells[1].Value.ToString() + "','" + dgvDatos.Rows[i].Cells[2].Value.ToString() + "','" + dgvDatos.Rows[i].Cells[3].Value.ToString() + "');";

                    querys = querys + query;

                }

                MySqlCommand mycomand = new MySqlCommand(querys, conectar);
                mycomand.ExecuteReader();
                MessageBox.Show("DATOS GUARDADOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private void dgvDatos_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            i = dgvDatos.CurrentCell.RowIndex;
            j = dgvDatos.CurrentCell.ColumnIndex;
            string strLimieInferior = string.Empty;
            string strLimiteSuperior = string.Empty;
            string strCuotaFija = string.Empty;
            string strPorcentaje = string.Empty;

            txtLimiteInferior.Text = dgvDatos.Rows[i].Cells[0].Value.ToString();
            txtLimiteSuperior.Text = dgvDatos.Rows[i].Cells[1].Value.ToString();
            txtCuotaFija.Text = dgvDatos.Rows[i].Cells[2].Value.ToString();
            txtPorcentaje.Text = dgvDatos.Rows[i].Cells[3].Value.ToString();

        }

        private void toolStripEditar_Click(object sender, EventArgs e)
        {
            string query = string.Empty;
            string strLimiteInferior = string.Empty;
            string strLimiteSuperior = string.Empty;
            string strCuotaFija = string.Empty;
            string strPorcentaje = string.Empty;
            string strAnio = string.Empty;
            string strMes = string.Empty;

            string strLimInf = string.Empty;
            string strLimSup = string.Empty;
            string strCuoFija = string.Empty;
            string strPorc = string.Empty;

            strAnio = cmbAnio.Text;
            strMes = cmbTablas.Text;
            strLimiteInferior = dgvDatos.Rows[i].Cells[0].Value.ToString();
            strLimiteSuperior = dgvDatos.Rows[i].Cells[1].Value.ToString();
            strCuotaFija = dgvDatos.Rows[i].Cells[2].Value.ToString();
            strPorcentaje = dgvDatos.Rows[i].Cells[3].Value.ToString();


            strLimInf = txtLimiteInferior.Text;
            strLimSup = txtLimiteSuperior.Text;
            strCuoFija = txtCuotaFija.Text;
            strPorc = txtPorcentaje.Text;

            query = "UPDATE  TABLAS  SET LIM_INFERIOR = '" + strLimInf + "',LIM_SUPERIOR = '" + strLimSup + "', CUOTA_FIJA = '" + strCuoFija + "' , " +
                "PORCENTAJE = '" + strPorc + "' where    ANO = '" + strAnio + "' AND MES = '" + strMes + "' AND LIM_INFERIOR = '" + strLimiteInferior + "' " +
                "AND LIM_SUPERIOR = '" + strLimiteSuperior + "' " + " AND CUOTA_FIJA = '" + strCuotaFija + "' " + " AND PORCENTAJE = '" + strPorcentaje + "'";

            conectar.Open();
             MySqlCommand mycomand = new MySqlCommand(query, conectar);
            mycomand.ExecuteReader();
         
            MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
          //  BuscarTabla();
        }
    }
}
