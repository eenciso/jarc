﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Facturacion.JARC
{
    public partial class frmCalculoImpuestos : Form
    {
        MySqlConnection conectar;
        public frmCalculoImpuestos()
        {
            InitializeComponent();
        }

        private void frmCalculoImpuestos_Load(object sender, EventArgs e)
        {
            AgregarRows();
            Conexion();
            CargarAnio();
        }
        private void AgregarRows()
        {
            for (int i = 0; i < 53; i++)
            {
                dgbClculoImpuestos.Rows.Add();
            }

            dgbClculoImpuestos.Rows[0].Cells[0].Value = "INGRESOS";
            dgbClculoImpuestos.Rows[1].Cells[0].Value = "INGRESOS DE PERIOD / ANT.";
            dgbClculoImpuestos.Rows[2].Cells[0].Value = "EGRESOS";
            dgbClculoImpuestos.Rows[3].Cells[0].Value = "DEDUCCIONES PERSONALES";
            dgbClculoImpuestos.Rows[4].Cells[0].Value = "PERDIDAS FISCALES";
            dgbClculoImpuestos.Rows[5].Cells[0].Value = "DEPRECIACION";
            dgbClculoImpuestos.Rows[6].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[7].Cells[0].Value = "BASE ISR";
            dgbClculoImpuestos.Rows[8].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[9].Cells[0].Value = "CALCULO IMPUESTO:";
            dgbClculoImpuestos.Rows[10].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[11].Cells[0].Value = "LIMITE INFERIOR";
            dgbClculoImpuestos.Rows[12].Cells[0].Value = "EXCEDENTE";
            dgbClculoImpuestos.Rows[13].Cells[0].Value = "BASE ISR";
            dgbClculoImpuestos.Rows[14].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[15].Cells[0].Value = "CUOTA FIJA";
            dgbClculoImpuestos.Rows[16].Cells[0].Value = "SOBRE EXCEDENTE";
            dgbClculoImpuestos.Rows[17].Cells[0].Value = "ISR NORMAL";
            dgbClculoImpuestos.Rows[18].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[19].Cells[0].Value = "ISR APLICABLE";
            dgbClculoImpuestos.Rows[20].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[21].Cells[0].Value = "ISR CAUSADO";
            dgbClculoImpuestos.Rows[22].Cells[0].Value = "DEC . ESTADIST. SAT";
            dgbClculoImpuestos.Rows[23].Cells[0].Value = "FOLIO";
            dgbClculoImpuestos.Rows[24].Cells[0].Value = "FECHA DE PRESENTAC.";
            dgbClculoImpuestos.Rows[25].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[26].Cells[0].Value = "SUMA ISR PAGADO ANTERIOR";
            dgbClculoImpuestos.Rows[27].Cells[0].Value = "ISR  X PAG (A FAVOR)";
            dgbClculoImpuestos.Rows[28].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[29].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[30].Cells[0].Value = "IVA TRASLADADO";
            dgbClculoImpuestos.Rows[31].Cells[0].Value = "IVA RETENIDO";
            dgbClculoImpuestos.Rows[32].Cells[0].Value = "IVA TRASLAD NETO";
            dgbClculoImpuestos.Rows[33].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[34].Cells[0].Value = "IVA ACREDITABLE MENSUAL";
            dgbClculoImpuestos.Rows[35].Cells[0].Value = "IVA A FAVOR AÑOS ANTERIORES";
            dgbClculoImpuestos.Rows[36].Cells[0].Value = "IVA A FAVOR MENSUAL";
            dgbClculoImpuestos.Rows[37].Cells[0].Value = "IVA ACREDITABLE";
            dgbClculoImpuestos.Rows[38].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[39].Cells[0].Value = "IVA POR PAGAR";
            dgbClculoImpuestos.Rows[40].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[41].Cells[0].Value = "IVA A FAVOR";
            dgbClculoImpuestos.Rows[42].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[43].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[44].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[45].Cells[0].Value = "DEC . ESTADIST. SAT";
            dgbClculoImpuestos.Rows[46].Cells[0].Value = "FOLIO";
            dgbClculoImpuestos.Rows[47].Cells[0].Value = "FECHA DE PRESENTAC.";
            dgbClculoImpuestos.Rows[48].Cells[0].Value = "";
            dgbClculoImpuestos.Rows[49].Cells[0].Value = "PAGADO EN DECLARACIONES:";
            dgbClculoImpuestos.Rows[50].Cells[0].Value = "ISR";
            dgbClculoImpuestos.Rows[51].Cells[0].Value = "IVA";
        }
        private void CargarAnio()
        {
            DateTime anio = DateTime.Today;

            for (int i = anio.Year; i >= 1900; i--)
            {
                cmbAnio.Items.Add(i);
            }

        }

        private void toolStripBuscar_Click(object sender, EventArgs e)
        {
            ValidaDatos();
        }

        private void Conexion()
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }
        private void ValidaDatos()
        {
            if (cmbAnio.Text != "")
            {
                BuscarCalculoImpuestos();
            }
            else
            {
                MessageBox.Show("DEDE REALIZAR UNA BUSQUEDA", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void BuscarCalculoImpuestos()
        {
            List<string> ListBox1 = new List<string>();
            List<string> ListBox2 = new List<string>();
            List<string> ListBox3 = new List<string>();
            List<string> ListBox4 = new List<string>();
            string strPorcentaje1 = string.Empty;
            
            string strConsulta1 = string.Empty;
            string strAcum = string.Empty;
            string strIngresos = string.Empty;
            string strEgresos = string.Empty;
            string strAno = string.Empty;
            string strMes = string.Empty;
            string strLimSup = string.Empty;
            string strCuota = string.Empty;
            string strLimInf = string.Empty;
            string strPorcentaje = string.Empty;
             
            decimal dISR = 0;
            decimal dLimInf = 0;
            decimal dLim = 0;
            decimal dCuota = 0;
            decimal dExcedente = 0;
            decimal dPorcentaje = 0;
            decimal dSobreExcedente = 0;

            strAno = cmbAnio.Text;
            //INGRESOS
            for (int i = 1; i < 13; i++)
            {
                strConsulta1 = "SELECT IMPORTE  FROM INGRESOSEGRESOSMENSUALES WHERE MES = " + i + " AND MENSUAL = 'E' AND ANO = " + strAno + ";";
                MySqlCommand mycomands1 = new MySqlCommand(strConsulta1, conectar);
                MySqlDataReader myreader1 = mycomands1.ExecuteReader();
                if (myreader1.Read())
                {
                    strIngresos = myreader1["IMPORTE"].ToString();
                }
                else
                {
                    strIngresos = "";
                }
                myreader1.Close();
                dgbClculoImpuestos.Rows[0].Cells[i].Value = strIngresos;


                //EGRESOS
                strConsulta1 = "SELECT IMPORTE  FROM INGRESOSEGRESOSMENSUALES WHERE MES = " + i + " AND MENSUAL = 'R' AND ANO =" + strAno + ";";
                MySqlCommand mycomands2 = new MySqlCommand(strConsulta1, conectar);
                MySqlDataReader myreader2 = mycomands2.ExecuteReader();
                if (myreader2.Read())
                {
                    strEgresos = myreader2["IMPORTE"].ToString();
                }
                else
                {
                    strEgresos = "";
                }
                myreader2.Close();
                dgbClculoImpuestos.Rows[2].Cells[i].Value = strEgresos;

                //INGRESOS DE PERIOD/ANT.

                strConsulta1 = " SELECT ACUM FROM ingresosegresosmensuales WHERE MES = " + i + "  AND MENSUAL = 'E';";
                MySqlCommand mycomands3 = new MySqlCommand(strConsulta1, conectar);
                MySqlDataReader myreader3 = mycomands3.ExecuteReader();
                if (myreader3.Read())
                {
                    strAcum = myreader3["ACUM"].ToString();
                }
                else
                {
                    strAcum = "";
                }
                myreader3.Close();
                dgbClculoImpuestos.Rows[1].Cells[i].Value = strAcum;

                //BASE ISR
                if (strAcum != "" && strIngresos != "" && strEgresos != "")
                {
                    dISR = Convert.ToDecimal(strAcum) + Convert.ToDecimal(strIngresos);
                    dISR = dISR - Convert.ToDecimal(strEgresos);
                    dgbClculoImpuestos.Rows[7].Cells[i].Value = dISR;
                    dgbClculoImpuestos.Rows[13].Cells[i].Value = dISR;
                }

                // LIMITE INFERIOR 
                //CUOTA FIJA
                strMes = dgbClculoImpuestos.Columns[i].HeaderText;
                strConsulta1 = " select LIM_SUPERIOR, CUOTA_FIJA ,LIM_INFERIOR, PORCENTAJE  from TABLAS WHERE ANO = " + strAno + " AND MES = '" + strMes + "';";
                MySqlCommand mycomands4 = new MySqlCommand(strConsulta1, conectar);
                MySqlDataReader myreader4 = mycomands4.ExecuteReader();
                while (myreader4.Read())
                {
                    int cont = 0;

                    ListBox1.Add(Convert.ToString(myreader4["LIM_SUPERIOR"]));
                    ListBox2.Add(Convert.ToString(myreader4["CUOTA_FIJA"]));
                    ListBox3.Add(Convert.ToString(myreader4["LIM_INFERIOR"]));
                    ListBox4.Add(Convert.ToString(myreader4["PORCENTAJE"]));
                    cont++;
                    for (int j = 0; j < ListBox1.Count; j++)
                    {
                        dLim = Convert.ToDecimal(ListBox1[j]);
                        dCuota = Convert.ToDecimal(ListBox2[j]);
                        strPorcentaje = ListBox4[j];
                        strPorcentaje1 = strPorcentaje.Split('%')[0].ToString();
                        dPorcentaje = Convert.ToDecimal(strPorcentaje1);
                        if (dISR <= dLim && dISR >= dCuota)
                        {
                            strLimInf = ListBox3[j];
                            dgbClculoImpuestos.Rows[11].Cells[i].Value = strLimInf;
                            dgbClculoImpuestos.Rows[15].Cells[i].Value = dCuota;
                        }

                    }
                }

                //EXCEDENTE
                if (dgbClculoImpuestos.Rows[11].Cells[i].Value != null && dgbClculoImpuestos.Rows[13].Cells[i].Value != null)
                {
                    dLimInf = Convert.ToDecimal(strLimInf);
                    dExcedente = dLimInf - dISR;
                    dgbClculoImpuestos.Rows[12].Cells[i].Value = dExcedente;
                    //SOBRE EXCEDENTE
                    dSobreExcedente = dExcedente * dPorcentaje;
                    dgbClculoImpuestos.Rows[16].Cells[i].Value = dExcedente;
                }
               
                myreader4.Close();
            }

        }
    }
}
