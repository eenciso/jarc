﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;
using Facturacion.Clases;
using System.IO;
using MySql.Data.MySqlClient;

namespace Facturacion.JARC
{
    public partial class frmFacturasEmitidas : Form
    {
        private String rutaXML;
        Factura facturaVo = new Factura();
        String strRuta = string.Empty;
        string strUUID = string.Empty;
        string strPolizas = string.Empty;
        string strConse = string.Empty;
        string strCliente = string.Empty;
        string sValue = string.Empty;
       string strAnioGeneral = string.Empty;
        string strMesGeneral = string.Empty;
        MySqlConnection conectar;
        public frmFacturasEmitidas(string strRutas, string strUUID1, string strPoliza, string strConsecutivo, string strClenites, string strAnio, string strMes)
        {
            strAnioGeneral = strAnio;
            strMesGeneral = strMes;
            strUUID = strUUID1;
            strRuta = strRutas;
            strPolizas = strPoliza;
            strConse = strConsecutivo;
            strCliente = strClenites;
            InitializeComponent();
            ConexionBD();
            Abrir();
        }

        private void toolBuscar_Click(object sender, EventArgs e)
        {
            // Buscar();
            //Abrir();
            Validar();

        }

        private void Validar()
        {
            if (txtPoliza.Text != "")
            {
                GuardarFacCodigo();
            }
            else
            {
                MessageBox.Show("DEDE INGRESAR TODOS LOS DATOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Abrir()
        {

            //XmlSerializer serial = new XmlSerializer(typeof(Comprobante));
            //FileStream fs = new FileStream("C:\\Users\\ruize\\Documents\\Facturas\\2018-02\\0C5D7BED-6D15-412A-BE83-7280EB684011.xml", FileMode.Open);
            //Comprobante ds = (Comprobante)serial.Deserialize(fs);
            ////dgvDatos.DataSource = ds;
            //MessageBox.Show(ds.Emisor.ToString());
            //fs.Close();
            // rutaXML = "C:\\Users\\ruize\\Documents\\Facturas\\2018-02\\0C5D7BED-6D15-412A-BE83-7280EB684011.xml";
            //rutaXML = "C:\\Users\\Jazmin\\OneDrive\\Documentos\\Facturas_Emitidas\\" + strRuta;


            rutaXML = "C:\\CapDigi\\CFDiDescargaXml\\Emitidos\\RACA490321B88\\" + strRuta;
            XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
            XmlTextReader reader = new XmlTextReader(@"" + rutaXML + "");
            Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
            facturaVo.DatosFactura = factura;


            //MessageBox.Show(factura.Emisor.Rfc.ToString()+ factura.Emisor.Nombre.ToString()); 
            // facturaVo.DatosFactura = factura;
            //txtRfcEmisor.Text = factura.Emisor.Rfc.ToString();
            //txtRegimenEmisor.Text = factura.Emisor.RegimenFiscal.ToString();
            //txtRfcRazonSocial.Text = factura.Emisor.Nombre.ToString();
            txtPoliza.Text = strPolizas;
            ExtraeComprobante();
            ExtraeDatosEmisor();
            ExtraeConceptos();
            ExtraeDatosReceptor();

        }

        private void ExtraeComprobante()
        {

            string version;
            string sello;
            string serie;
            string folio;
            DateTime fecha;
            string formaDePago;
            string noCertificado;
            string certificado;
            decimal subTotal;
            decimal descuento;
            decimal total;
            string metodoDePago;
            string tipodeComprobante;
            string moneda;

            version = facturaVo.DatosFactura.Version;
            sello = facturaVo.DatosFactura.Sello;
            serie = facturaVo.DatosFactura.Serie;
            folio = facturaVo.DatosFactura.Folio;
            fecha = facturaVo.DatosFactura.Fecha;
            formaDePago = facturaVo.DatosFactura.FormaPagoSpecified.ToString();
            noCertificado = facturaVo.DatosFactura.NoCertificado;
            certificado = facturaVo.DatosFactura.Certificado;
            subTotal = facturaVo.DatosFactura.SubTotal;
            descuento = facturaVo.DatosFactura.Descuento;
            total = facturaVo.DatosFactura.Total;
            metodoDePago = facturaVo.DatosFactura.MetodoPagoSpecified.ToString();
            tipodeComprobante = facturaVo.DatosFactura.TipoDeComprobante.ToString();
            moneda = facturaVo.DatosFactura.Moneda.ToString();
            this.textConsecutivo.Text = strConse;
            AsignarValoresComprobante(version, sello, serie, folio, fecha, formaDePago, noCertificado, certificado,
                subTotal, descuento, total, metodoDePago, tipodeComprobante, moneda);
        }

        private void ExtraeDatosEmisor()
        {
            /////////////////////////////////////////
            //////          Datos Emisor        /////
            /////////////////////////////////////////

            string rfcEmisor;
            string razonSocialEmisor;
            string regimenEmisor;

            /////////////////////////////////////////
            //////     Domicilio Fiscal         /////
            /////////////////////////////////////////

            //string calleDomFis;
            //string noExteriorDomFis;
            //string coloniaDomFis;
            //string localidadDomFis;
            //string municipioDomFis;
            //string estadoDomFis;
            //string paisDomFis;
            //string codigoPostalDomFis;


            /////////////////////////////////////////
            //////     Expedido en              /////
            /////////////////////////////////////////

            //string calleExEn;
            //string noExteriorExEn;
            //string coloniaExEn;
            //string localidadExEn;
            //string municipioExEn;
            //string estadoDomExEn;
            //string paisDomExEn;
            //string codigoPostalExEn;


            rfcEmisor = facturaVo.DatosFactura.Emisor.Rfc;
            razonSocialEmisor = facturaVo.DatosFactura.Emisor.Nombre;
            regimenEmisor = facturaVo.DatosFactura.Emisor.RegimenFiscal.ToString();

            //calleDomFis = facturaVo.DatosFactura.Emisor. DomicilioFiscal.calle;
            //noExteriorDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.noExterior;
            //coloniaDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.colonia;
            //localidadDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.localidad;
            //municipioDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.municipio;
            //estadoDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.estado;
            //paisDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.pais;
            //codigoPostalDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.codigoPostal;



            //calleExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.calle;
            //noExteriorExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.noExterior;
            //coloniaExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.colonia;
            //localidadExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.localidad;
            //municipioExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.municipio;
            //estadoDomExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.estado;
            //paisDomExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.pais;
            //codigoPostalExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.codigoPostal;

            AsignarValoresDatosEmisor(
                //paisDomFis, estadoDomFis, municipioDomFis,
                //coloniaDomFis, localidadDomFis, calleDomFis, noExteriorDomFis, codigoPostalDomFis,
                //calleExEn, noExteriorExEn, coloniaExEn, localidadExEn, municipioExEn, estadoDomExEn,
                //paisDomExEn, codigoPostalExEn, 
                rfcEmisor, razonSocialEmisor, regimenEmisor);


        }



        private void ExtraeDatosReceptor()
        {
            //string calleReceptor;
            //string noExteriorReceptor;
            //string coloniaReceptor;
            //string localidadReceptor;
            //string municipioReceptor;
            //string estadoDomReceptor;
            //string paisDomReceptor;
            //string codigoPostalReceptor;

            //calleReceptor = facturaVo.DatosFactura.Receptor.Domicilio.calle;
            //noExteriorReceptor = facturaVo.DatosFactura.Receptor.Domicilio.noExterior;
            //coloniaReceptor = facturaVo.DatosFactura.Receptor.Domicilio.colonia;
            //localidadReceptor = facturaVo.DatosFactura.Receptor.Domicilio.localidad;
            //municipioReceptor = facturaVo.DatosFactura.Receptor.Domicilio.municipio;
            //estadoDomReceptor = facturaVo.DatosFactura.Receptor.Domicilio.estado;
            //paisDomReceptor = facturaVo.DatosFactura.Receptor.Domicilio.pais;
            //codigoPostalReceptor = facturaVo.DatosFactura.Receptor.Domicilio.codigoPostal;

            //AsignarValoresDatosReceptor(paisDomReceptor, estadoDomReceptor, municipioReceptor, coloniaReceptor,
            //    localidadReceptor, calleReceptor, noExteriorReceptor, codigoPostalReceptor);

            string receptorRFC;
            string receptorNombre;
            string receptorUsoCFDI;
            string usoCFID;
            receptorRFC = facturaVo.DatosFactura.Receptor.Rfc;
            receptorNombre = facturaVo.DatosFactura.Receptor.Nombre;
            receptorUsoCFDI = facturaVo.DatosFactura.Receptor.NumRegIdTrib;
            usoCFID = facturaVo.DatosFactura.Receptor.NumRegIdTrib;
            AsignarValoresDatosReceptor(receptorRFC, receptorNombre, receptorUsoCFDI);

        }



        private void ExtraeConceptos()
        {
            int cantidadConseptos = facturaVo.DatosFactura.Conceptos.Length;
            string[] noIdentificacion = new string[cantidadConseptos];
            string[] descripcion = new string[cantidadConseptos];
            decimal[] cantidad = new decimal[cantidadConseptos];
            string[] unidad = new string[cantidadConseptos];
            decimal[] valorUnitario = new decimal[cantidadConseptos]; ;
            decimal[] importe = new decimal[cantidadConseptos];

            decimal totalImpuestosRetenidos;
            decimal totalImpuestosTrasladados;

            int i;

            for (i = 0; i < cantidadConseptos; i++)
            {
                noIdentificacion[i] = facturaVo.DatosFactura.Conceptos[i].NoIdentificacion;
                descripcion[i] = facturaVo.DatosFactura.Conceptos[i].Descripcion;
                cantidad[i] = facturaVo.DatosFactura.Conceptos[i].Cantidad;
                unidad[i] = facturaVo.DatosFactura.Conceptos[i].Unidad;
                valorUnitario[i] = facturaVo.DatosFactura.Conceptos[i].ValorUnitario;
                importe[i] = facturaVo.DatosFactura.Conceptos[i].Importe;


            }

            AsignarValoresConseptos(cantidadConseptos, noIdentificacion, descripcion, cantidad, unidad, valorUnitario, importe);



            /////////////////////////////////////////
            //////      Estrae Inpuestos        /////
            /////////////////////////////////////////
            // totalImpuestosRetenidos = facturaVo.DatosFactura.Impuestos.TotalImpuestosRetenidos;
            //totalImpuestosTrasladados = facturaVo.DatosFactura.Impuestos.TotalImpuestosTrasladados;

            //AsignaInpuestos(totalImpuestosRetenidos, totalImpuestosTrasladados);

        }

        private void AsignarValoresDatosEmisor(string rfcEmisor, string razonSocialEmisor, string regimenEmisor)
        {

            /////////////////////////////////////////
            //////     Set caja de Texto        /////
            /////////////////////////////////////////

            this.txtRfcEmisor.Text = rfcEmisor;
            this.txtRfcRazonSocial.Text = strCliente;
            this.txtRegimenEmisor.Text = regimenEmisor;
            this.txtUsoCFDI.Text = strUUID;

            ////////////////////////////////////////////
            //////        Domicilio Fiscal        //////
            /////             Atributo            //////
            ////////////////////////////////////////////

            dvgDomFiscal.Rows.Add("Pais");
            dvgDomFiscal.Rows.Add("Estado");
            dvgDomFiscal.Rows.Add("Municipio");
            dvgDomFiscal.Rows.Add("Colonia");
            dvgDomFiscal.Rows.Add("Localidad");
            dvgDomFiscal.Rows.Add("Calle");
            dvgDomFiscal.Rows.Add("Num. Exterior");
            dvgDomFiscal.Rows.Add("Codigo Postal");


            //////////////////////////////////////////
            //////        Domicilio Fiscal       /////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            //dvgDomFiscal.Rows[0].Cells[1].Value = paisDomFis;
            //dvgDomFiscal.Rows[1].Cells[1].Value = estadoDomFis;
            //dvgDomFiscal.Rows[2].Cells[1].Value = municipioDomFis;
            //dvgDomFiscal.Rows[3].Cells[1].Value = coloniaDomFis;
            //dvgDomFiscal.Rows[4].Cells[1].Value = localidadDomFis;
            //dvgDomFiscal.Rows[5].Cells[1].Value = calleDomFis;
            //dvgDomFiscal.Rows[6].Cells[1].Value = noExteriorDomFis;
            //dvgDomFiscal.Rows[7].Cells[1].Value = codigoPostalDomFis;


            ////////////////////////////////////////////
            //////         Expedido En            //////
            /////            Atributo             //////
            ////////////////////////////////////////////

            dvgExpEn.Rows.Add("Pais");
            dvgExpEn.Rows.Add("Estado");
            dvgExpEn.Rows.Add("Municipio");
            dvgExpEn.Rows.Add("Colonia");
            dvgExpEn.Rows.Add("Localidad");
            dvgExpEn.Rows.Add("Calle");
            dvgExpEn.Rows.Add("Num. Exterior");
            dvgExpEn.Rows.Add("Codigo Postal");


            //////////////////////////////////////////
            //////        Expedido En           /////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            //dvgExpEn.Rows[0].Cells[1].Value = paisDomExEn;
            //dvgExpEn.Rows[1].Cells[1].Value = estadoDomExEn;
            //dvgExpEn.Rows[2].Cells[1].Value = municipioExEn;
            //dvgExpEn.Rows[3].Cells[1].Value = coloniaExEn;
            //dvgExpEn.Rows[4].Cells[1].Value = localidadExEn;
            //dvgExpEn.Rows[5].Cells[1].Value = calleExEn;
            //dvgExpEn.Rows[6].Cells[1].Value = noExteriorExEn;
            //dvgExpEn.Rows[7].Cells[1].Value = codigoPostalExEn;


        }


        private void Buscar()
        {

            LeerXML();
            //ExtraeComprobante();

        }

        private void LeerXML()
        {
            //string strRuta = string.Empty;

            //strRuta = "C:\\Users\\ruize\\Documents\\Facturas\\2018-02\\" + "0C5D7BED-6D15-412A-BE83-7280EB684011" + ".xml";
            //// strRuta = rutaXML.Replace("\\", "/");
            //XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
            //XmlTextReader reader = new XmlTextReader(@"" + strRuta + "");
            //Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
            //facturaVo.DatosFactura = factura;

            //while (reader.Read())
            //{
            //    if (reader.NodeType == XmlNodeType.Element)
            //    {


            //    }
            //}


            XmlSerializer serial = new XmlSerializer(typeof(Comprobante));
            FileStream fs = new FileStream("C:\\Users\\ruize\\Documents\\Facturas\\2018-02\\0C5D7BED-6D15-412A-BE83-7280EB684011" + ".xml", FileMode.Open);

            Comprobante ds = (Comprobante)serial.Deserialize(fs);
            fs.Close();

        }



        private void AsignarValoresComprobante(string version, string sello, string serie, string folio, DateTime fecha,
            string formaDePago, string noCertificado, string certificado, decimal subTotal, decimal descuento, decimal total,
            string metodoDePago, string tipodeComprobante, string moneda)
        {


            /////////////////////////////////////////
            //////     Columna Atributos        /////
            /////////////////////////////////////////

            dgvComprobante.Rows.Add("Versión");
            dgvComprobante.Rows.Add("Serie");
            dgvComprobante.Rows.Add("Folio");
            dgvComprobante.Rows.Add("Fecha");
            dgvComprobante.Rows.Add("Sello");
            dgvComprobante.Rows.Add("Forma de pago");
            dgvComprobante.Rows.Add("No Certificado");
            dgvComprobante.Rows.Add("Certificado");
            dgvComprobante.Rows.Add("Subtotal");
            dgvComprobante.Rows.Add("Descuento");
            dgvComprobante.Rows.Add("Total");
            dgvComprobante.Rows.Add("Metodo de Pago");
            dgvComprobante.Rows.Add("Tipo de Comprobante");
            dgvComprobante.Rows.Add("Moneda");

            /////////////////////////////////////////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            dgvComprobante.Rows[0].Cells[1].Value = version;
            dgvComprobante.Rows[1].Cells[1].Value = serie;
            dgvComprobante.Rows[2].Cells[1].Value = folio;
            dgvComprobante.Rows[3].Cells[1].Value = fecha;
            dgvComprobante.Rows[4].Cells[1].Value = sello;
            dgvComprobante.Rows[5].Cells[1].Value = formaDePago;
            dgvComprobante.Rows[6].Cells[1].Value = noCertificado;
            dgvComprobante.Rows[7].Cells[1].Value = certificado;
            dgvComprobante.Rows[8].Cells[1].Value = subTotal;
            dgvComprobante.Rows[9].Cells[1].Value = descuento;
            dgvComprobante.Rows[10].Cells[1].Value = total;
            dgvComprobante.Rows[11].Cells[1].Value = metodoDePago;
            dgvComprobante.Rows[12].Cells[1].Value = tipodeComprobante;
            dgvComprobante.Rows[13].Cells[1].Value = moneda;
            txtFolioFiscal.Text = folio.ToString();
            txtFechaComprobante.Text = fecha.ToString();

        }
        private void CargarCodigoContables()
        {
            string query = string.Empty;
            DataTable ds = new DataTable();
            DataTable dt = new DataTable();
            query = "SELECT MAYOR,CTA,SCTA,DESCRIPCION  FROM CATCONTABILIDAD;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds);
            conectar.Close();
            dt.Columns.Add("id");
            dt.Columns.Add("cod");

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                DataRow row = dt.NewRow();

                row["id"] = i;
                row["cod"] = ds.Rows[i].ItemArray[0].ToString() + "-" + ds.Rows[i].ItemArray[1].ToString() + "-" + ds.Rows[i].ItemArray[2].ToString() + "-" + ds.Rows[i].ItemArray[3].ToString();
                dt.Rows.Add(row);

            }
            cmbCont.ValueMember = "id";
            cmbCont.DisplayMember = "cod";
            cmbCont.DataSource = dt;

        }

        private void AsignarValoresDatosReceptor(String receptorRFC, String receptorNombre, String receptorUsoCFDI)
        {
            //private void AsignarValoresDatosReceptor(String paisDomReceptor, String estadoDomReceptor, String municipioReceptor, String coloniaReceptor,
            //       String localidadReceptor, String calleReceptor, String noExteriorReceptor, String codigoPostalReceptor)
            //{
            /////////////////////////////////////////////
            //////         Datos Receptor          //////
            /////            Atributo             //////
            ////////////////////////////////////////////

            //dgwReceptor.Rows.Add("Pais");
            //dgwReceptor.Rows.Add("Estado");
            //dgwReceptor.Rows.Add("Municipio");
            //dgwReceptor.Rows.Add("Colonia");
            //dgwReceptor.Rows.Add("Localidad");
            //dgwReceptor.Rows.Add("Calle");
            //dgwReceptor.Rows.Add("Num. Exterior");
            //dgwReceptor.Rows.Add("Codigo Postal");



            //////////////////////////////////////////
            //////        Datos Receptor        /////
            //////        Columna Valor         /////
            /////////////////////////////////////////

            //dgwReceptor.Rows[0].Cells[1].Value = paisDomReceptor;
            //dgwReceptor.Rows[1].Cells[1].Value = estadoDomReceptor;
            //dgwReceptor.Rows[2].Cells[1].Value = municipioReceptor;
            //dgwReceptor.Rows[3].Cells[1].Value = coloniaReceptor;
            //dgwReceptor.Rows[4].Cells[1].Value = localidadReceptor;
            //dgwReceptor.Rows[5].Cells[1].Value = calleReceptor;
            //dgwReceptor.Rows[6].Cells[1].Value = noExteriorReceptor;
            //dgwReceptor.Rows[7].Cells[1].Value = codigoPostalReceptor;

        }




        private void AsignarValoresConseptos(int cantidadConseptos, string[] noIdentificacion, string[] descripcion,
            decimal[] cantidad, string[] unidad, decimal[] valorUnitario, decimal[] importe)
        {


            int i;
            for (i = 0; i < cantidadConseptos; i++)
            {
                dgwConseptos.Rows.Add();
                dgwConseptos.Rows[i].Cells[0].Value = noIdentificacion[i];
                dgwConseptos.Rows[i].Cells[1].Value = descripcion[i];
                dgwConseptos.Rows[i].Cells[2].Value = cantidad[i];
                dgwConseptos.Rows[i].Cells[3].Value = unidad[i];
                dgwConseptos.Rows[i].Cells[4].Value = valorUnitario[i];
                dgwConseptos.Rows[i].Cells[5].Value = importe[i];
            }


        }

        private void frmFacturasEmitidas_Load(object sender, EventArgs e)
        {
            //ConexionBD();
            CargarCodigoContable();
            CargarCodigoContables();
        }

        private void CargarCodigoContable()
        {
            string query = string.Empty;
            DataSet ds = new DataSet();

            query = "SELECT MAYOR,CTA,SCTA,DESCRIPCION  FROM CATCONTABILIDAD;";
            //conectar.Open();
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds, "MAYOR");
            conectar.Close();
            foreach (DataRow row in ds.Tables["MAYOR"].Rows)
            {
                cmbCodigosCont.Items.Add(row.ItemArray[0] + "-" + row.ItemArray[1] + "-" + row.ItemArray[2] + "-" + row.ItemArray[3]);
            }
        }
        private void ConexionBD()
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }

        private void GuardarFacCodigo()
        {
            string query = string.Empty;
            string strMayor = string.Empty;
            string strPoliza = string.Empty;
            string strCTA = string.Empty;
            string strSCTA = string.Empty;
            string strCodigo = string.Empty;
            string strUUID2 = string.Empty;
            string strTipo = string.Empty;
            string strFecha = string.Empty;
            string strIdentificador = string.Empty;

            strPoliza = txtPoliza.Text;
            strFecha = dtFecha.Text;
            strCodigo = cmbCodigosCont.Text;
            if (strCodigo != "")
            {

                String[] substrings = strCodigo.Split('-');
                String[] substring = strUUID.Split('.');
                strUUID2 = substring[0];



                if (substrings.Length > 0)
                    strMayor = substrings[0];
                if (substrings.Length > 0)
                    strCTA = substrings[1];
                if (substrings.Length > 0)
                    strSCTA = substrings[2];
                int intNum = BuscarConcepto(strUUID2);

                if (intNum == 0)
                {

                    query = "INSERT INTO FACTURASCODIGOS VALUE(" + "'" + strMayor + "','" + strCTA + "','" + strSCTA + "','" + strPoliza + "','" + strUUID2 + "','" + strFecha + "','E');";
                    query = query + " UPDATE FACEMITIDAS SET CODCONT = " + strPoliza + " WHERE UUID = '" + strUUID + "';";
                    conectar.Open();
                    MySqlCommand mycomand = new MySqlCommand(query, conectar);
                    mycomand.ExecuteReader();
                    conectar.Close();


                    MessageBox.Show("DATOS GUARDADOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {


                for (int i = 0; i < dgwConseptos.Rows.Count; i++)
                {

                   

                    //int intIndex = cmbCont.Index;
                    strCodigo = sValue;
                    String[] substrings = strCodigo.Split('-');
                    String[] substring = strUUID.Split('.');
                    strUUID2 = substring[0];
                    if(dgwConseptos.Rows[i].Cells[0].Value != null)
                    strIdentificador = dgwConseptos.Rows[i].Cells[0].Value.ToString();
                    // string.IsNullOrEmpty(dgwConseptos.Rows[i].Cells[0] ? "" : substrings[2];


                    if (substrings.Length > 0)
                        strMayor = substrings[0];
                    if (substrings.Length > 0)
                        strCTA = substrings[1];
                    if (substrings.Length > 0)
                        strSCTA = substrings[2];
                    int intNum = BuscarConceptos(strUUID2, strIdentificador);
                    if (intNum == 0)
                    {

                        query = "INSERT INTO CONCEPTOSGENERALES VALUE(" + "'" + strUUID2 + "','" + strIdentificador + "','" + strMayor + "','" + strCTA + "','" + strSCTA + "','" + strFecha + "','E');";
                        query = query + " UPDATE FACEMITIDAS SET CODCONT = " + strPoliza + " WHERE UUID = '" + strUUID + "';";
                        conectar.Open();
                        MySqlCommand mycomand = new MySqlCommand(query, conectar);
                        mycomand.ExecuteReader();
                        conectar.Close();

                    }
                }
                MessageBox.Show("DATOS GUARDADOS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }






            //  conectar.Clone();


            //INSERT INTO FACTURASCODIGOS VALUE('902','00','000','CUENTAS DE ORDEN ACREEDORAS');
        }

        private int BuscarConceptos(string strUUID2, string strIdentificador)
        {
            string query = string.Empty;
            int intNum = 0;
            DataTable ds = new DataTable();
            query = "SELECT *  FROM CONCEPTOSGENERALES WHERE CFDI = '" + strUUID2 + "' AND NO_IDENTIFICADOR ='" + strIdentificador + "' AND TIPO = 'E';";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            conectar.Close();

            myAdapter.Fill(ds);

            if (ds.Rows.Count > 0)
            {
                intNum = ds.Rows.Count;
            }
            return intNum;


        }

        private int BuscarConcepto(string strUUID2)
        {
            string query = string.Empty;
            int intNum = 0;
            DataTable ds = new DataTable();
            query = "SELECT *  FROM FACTURASCODIGOS WHERE UUID = '" + strUUID2 + "';";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds);
            conectar.Close();
            if (ds.Rows.Count > 0)
            {
                intNum = ds.Rows.Count;
            }
            return intNum;
        }

        

        private void dgwConseptos_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            //frmCodigos frmCodigoInterno = new frmCodigos();
            //frmCodigoInterno.ShowDialog();
            // dgwConseptos.Rows[0].Cells["cmbCom"].FindControl("TextBox");
            //if (dgwConseptos.CurrentRow != null)
            //{
            //    DataGridViewRow dr = dgwConseptos.CurrentRow;
            //    Convert.ToInt32(dr.Cells["cmbCont"].Value == DBNull.Value ? "0" : dr.Cells["cmbCont"].Value);
            //}
        }


        private void dgwConseptos_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
             

            //private void ExtraeComprobante()
            //{

            //    string version;
            //    string sello;
            //    string serie;
            //    string folio;
            //    DateTime fecha;
            //    string formaDePago;
            //    string noCertificado;
            //    string certificado;
            //    decimal subTotal;
            //    decimal descuento;
            //    decimal total;
            //    string metodoDePago;
            //    string tipodeComprobante;
            //    string moneda;
            //    string lugardeExpedicion;
            //    string numCtaPago;

            //    version = facturaVo.DatosFactura.version;
            //    sello = facturaVo.DatosFactura.sello;
            //    serie = facturaVo.DatosFactura.serie;
            //    folio = facturaVo.DatosFactura.folio;
            //    fecha = facturaVo.DatosFactura.fecha;
            //    formaDePago = facturaVo.DatosFactura.formaDePago;
            //    noCertificado = facturaVo.DatosFactura.noCertificado;
            //    certificado = facturaVo.DatosFactura.certificado;
            //    subTotal = facturaVo.DatosFactura.subTotal;
            //    descuento = facturaVo.DatosFactura.descuento;
            //    total = facturaVo.DatosFactura.total;
            //    metodoDePago = facturaVo.DatosFactura.metodoDePago;
            //    tipodeComprobante = facturaVo.DatosFactura.tipoDeComprobante.ToString();
            //    moneda = facturaVo.DatosFactura.Moneda;
            //    lugardeExpedicion = facturaVo.DatosFactura.LugarExpedicion;
            //    numCtaPago = facturaVo.DatosFactura.NumCtaPago.ToString();

            //    AsignarValoresComprobante(version, sello, serie, folio, fecha, formaDePago, noCertificado, certificado,
            //        subTotal, descuento, total, metodoDePago, tipodeComprobante, moneda, lugardeExpedicion, numCtaPago);
            //}

            //private void ExtraeDatosEmisor()
            //{
            //    /////////////////////////////////////////
            //    //////          Datos Emisor        /////
            //    /////////////////////////////////////////

            //    string rfcEmisor;
            //    string razonSocialEmisor;
            //    string regimenEmisor;

            //    /////////////////////////////////////////
            //    //////     Domicilio Fiscal         /////
            //    /////////////////////////////////////////

            //    string calleDomFis;
            //    string noExteriorDomFis;
            //    string coloniaDomFis;
            //    string localidadDomFis;
            //    string municipioDomFis;
            //    string estadoDomFis;
            //    string paisDomFis;
            //    string codigoPostalDomFis;


            //    /////////////////////////////////////////
            //    //////     Expedido en              /////
            //    /////////////////////////////////////////

            //    string calleExEn;
            //    string noExteriorExEn;
            //    string coloniaExEn;
            //    string localidadExEn;
            //    string municipioExEn;
            //    string estadoDomExEn;
            //    string paisDomExEn;
            //    string codigoPostalExEn;


            //    rfcEmisor = facturaVo.DatosFactura.Emisor.rfc;
            //    razonSocialEmisor = facturaVo.DatosFactura.Emisor.nombre;
            //    regimenEmisor = facturaVo.DatosFactura.Emisor.RegimenFiscal[0].Regimen;

            //    calleDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.calle;
            //    noExteriorDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.noExterior;
            //    coloniaDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.colonia;
            //    localidadDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.localidad;
            //    municipioDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.municipio;
            //    estadoDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.estado;
            //    paisDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.pais;
            //    codigoPostalDomFis = facturaVo.DatosFactura.Emisor.DomicilioFiscal.codigoPostal;



            //    calleExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.calle;
            //    noExteriorExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.noExterior;
            //    coloniaExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.colonia;
            //    localidadExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.localidad;
            //    municipioExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.municipio;
            //    estadoDomExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.estado;
            //    paisDomExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.pais;
            //    codigoPostalExEn = facturaVo.DatosFactura.Emisor.ExpedidoEn.codigoPostal;

            //    AsignarValoresDatosEmisor(paisDomFis, estadoDomFis, municipioDomFis,
            //        coloniaDomFis, localidadDomFis, calleDomFis, noExteriorDomFis, codigoPostalDomFis,
            //        calleExEn, noExteriorExEn, coloniaExEn, localidadExEn, municipioExEn, estadoDomExEn,
            //        paisDomExEn, codigoPostalExEn, rfcEmisor, razonSocialEmisor, regimenEmisor);


            //}

            //private void AsignarValoresDatosEmisor(string paisDomFis, string estadoDomFis, string municipioDomFis,
            //   string coloniaDomFis, string localidadDomFis, string calleDomFis, string noExteriorDomFis, string codigoPostalDomFis,
            //   string calleExEn, string noExteriorExEn, string coloniaExEn, string localidadExEn, string municipioExEn,
            //   string estadoDomExEn, string paisDomExEn, string codigoPostalExEn, string rfcEmisor,
            //   string razonSocialEmisor, string regimenEmisor)
            //{

            //    /////////////////////////////////////////
            //    //////     Set caja de Texto        /////
            //    /////////////////////////////////////////

            //    this.txtRfcEmisor.Text = rfcEmisor;
            //    this.txtRfcRazonSocial.Text = razonSocialEmisor;
            //    this.txtRegimenEmisor.Text = regimenEmisor;


            //    ////////////////////////////////////////////
            //    //////        Domicilio Fiscal        //////
            //    /////             Atributo            //////
            //    ////////////////////////////////////////////

            //    dvgDomFiscal.Rows.Add("Pais");
            //    dvgDomFiscal.Rows.Add("Estado");
            //    dvgDomFiscal.Rows.Add("Municipio");
            //    dvgDomFiscal.Rows.Add("Colonia");
            //    dvgDomFiscal.Rows.Add("Localidad");
            //    dvgDomFiscal.Rows.Add("Calle");
            //    dvgDomFiscal.Rows.Add("Num. Exterior");
            //    dvgDomFiscal.Rows.Add("Codigo Postal");


            //    //////////////////////////////////////////
            //    //////        Domicilio Fiscal       /////
            //    //////        Columna Valor         /////
            //    /////////////////////////////////////////

            //    dvgDomFiscal.Rows[0].Cells[1].Value = paisDomFis;
            //    dvgDomFiscal.Rows[1].Cells[1].Value = estadoDomFis;
            //    dvgDomFiscal.Rows[2].Cells[1].Value = municipioDomFis;
            //    dvgDomFiscal.Rows[3].Cells[1].Value = coloniaDomFis;
            //    dvgDomFiscal.Rows[4].Cells[1].Value = localidadDomFis;
            //    dvgDomFiscal.Rows[5].Cells[1].Value = calleDomFis;
            //    dvgDomFiscal.Rows[6].Cells[1].Value = noExteriorDomFis;
            //    dvgDomFiscal.Rows[7].Cells[1].Value = codigoPostalDomFis;


            //    ////////////////////////////////////////////
            //    //////         Expedido En            //////
            //    /////            Atributo             //////
            //    ////////////////////////////////////////////

            //    dvgExpEn.Rows.Add("Pais");
            //    dvgExpEn.Rows.Add("Estado");
            //    dvgExpEn.Rows.Add("Municipio");
            //    dvgExpEn.Rows.Add("Colonia");
            //    dvgExpEn.Rows.Add("Localidad");
            //    dvgExpEn.Rows.Add("Calle");
            //    dvgExpEn.Rows.Add("Num. Exterior");
            //    dvgExpEn.Rows.Add("Codigo Postal");


            //    //////////////////////////////////////////
            //    //////        Expedido En           /////
            //    //////        Columna Valor         /////
            //    /////////////////////////////////////////

            //    dvgExpEn.Rows[0].Cells[1].Value = paisDomExEn;
            //    dvgExpEn.Rows[1].Cells[1].Value = estadoDomExEn;
            //    dvgExpEn.Rows[2].Cells[1].Value = municipioExEn;
            //    dvgExpEn.Rows[3].Cells[1].Value = coloniaExEn;
            //    dvgExpEn.Rows[4].Cells[1].Value = localidadExEn;
            //    dvgExpEn.Rows[5].Cells[1].Value = calleExEn;
            //    dvgExpEn.Rows[6].Cells[1].Value = noExteriorExEn;
            //    dvgExpEn.Rows[7].Cells[1].Value = codigoPostalExEn;


            //}



        }

        private void dgwConseptos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        
            
        }

        private void dgwConseptos_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dgwConseptos.CurrentRow != null)
            {
                DataGridViewRow dr = dgwConseptos.CurrentRow;
                Convert.ToInt32(dr.Cells["cmbCont"].Value == DBNull.Value ? "0" : dr.Cells["cmbCont"].Value);
            }
        }

        private void dgwConseptos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if(dgwConseptos.CurrentCell.ColumnIndex == 6 && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                if(comboBox != null)
                {
                    comboBox.SelectedIndexChanged -= new
                         EventHandler(comboBox_SelectedChanged);

                    comboBox.SelectedIndexChanged += new
                         EventHandler(comboBox_SelectedChanged);
                }
            }
        }
        void comboBox_SelectedChanged(object sender , EventArgs e)
        {
            ComboBox comboBox = sender as  ComboBox;

            int selectedIndex = comboBox.SelectedIndex;
            Object selectedItem = comboBox.SelectedItem;
            
            DataRowView oDataRowView = comboBox.SelectedItem as DataRowView;

            if (oDataRowView != null)
            {
                sValue = oDataRowView.Row["cod"] as string;
            }
        }

        private void frmFacturasEmitidas_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogo = MessageBox.Show("¿Desea cerrar el programa?",
             "Cerrar el programa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogo == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;

                frmEmitidas frmR = new frmEmitidas(strAnioGeneral, strMesGeneral, "ok");
                frmR.Hide();
            }
        }
    }
    }
