﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Facturacion.JARC;
namespace Facturacion
{
    public partial class frmPrincipalFacturacion : Form
    {
        public frmPrincipalFacturacion()
        {
            InitializeComponent();
        }

        private void toolDescargaFac_Click(object sender, EventArgs e)
        {
            //frmDescargaFacturas frmDesFac = new frmDescargaFacturas();
            //frmDesFac.MdiParent = this;
            //frmDesFac.Show();

            frmBuscarXML frmXML = new frmBuscarXML();
            frmXML.MdiParent = this;
            frmXML.Show();

        }

        private void toolStripTablas_Click(object sender, EventArgs e)
        {
            frmTablas frmTab = new frmTablas();
            frmTab.MdiParent = this;
            frmTab.Show();
        }
        
        private void btnBusEmitidas_Click(object sender, EventArgs e)
        {
            frmEmitidas frmEmi = new frmEmitidas("","","");
            frmEmi.MdiParent = this;
            frmEmi.Show();
        }

        private void btnusRec_Click(object sender, EventArgs e)
        {
            frmRecibidas frmRec = new frmRecibidas("","","");
            frmRec.MdiParent = this;
            frmRec.Show();
        }

        private void btnCalcImpuestos_Click(object sender, EventArgs e)
        {
            frmCalculoImpuestos frmCal = new frmCalculoImpuestos();
            frmCal.MdiParent = this;
            frmCal.Show();
        }

        private void btnMensuales_Click(object sender, EventArgs e)
        {
            frmIngEgrMensuales frmMens = new frmIngEgrMensuales();
            frmMens.MdiParent = this;
            frmMens.Show();
        }

        private void toolStripCatCont_Click(object sender, EventArgs e)
        {
            frmCatContabilidad frmCatCont = new frmCatContabilidad();
            frmCatCont.MdiParent = this;
            frmCatCont.Show();
        }

        private void btnCodigoInterno_Click(object sender, EventArgs e)
        {
            frmCatalogoCodigoInterno frmCatInterno = new frmCatalogoCodigoInterno();
            frmCatInterno.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //Form1 F = new Form1();
            //F.Show();
            frmCatMaterias frmMaterias = new frmCatMaterias();
            frmMaterias.MdiParent = this;
            frmMaterias.Show();

        }
    }
}
