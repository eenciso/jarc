﻿namespace Facturacion.JARC
{
    partial class frmFacRecibidas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFacRecibidas));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolGuardar = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabXml = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvComprobante = new System.Windows.Forms.DataGridView();
            this.colAtributo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gbExEn = new System.Windows.Forms.GroupBox();
            this.dvgExpEn = new System.Windows.Forms.DataGridView();
            this.colAtributoExpEn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValorExpEn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbDomFiscal = new System.Windows.Forms.GroupBox();
            this.dvgDomFiscal = new System.Windows.Forms.DataGridView();
            this.colAtributoDomFis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValorDomFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRegimenEmisor = new System.Windows.Forms.TextBox();
            this.txtRfcRazonSocial = new System.Windows.Forms.TextBox();
            this.txtRfcEmisor = new System.Windows.Forms.TextBox();
            this.lbRegimen = new System.Windows.Forms.Label();
            this.lbRazonSocial = new System.Windows.Forms.Label();
            this.lbRFC = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgwReceptor = new System.Windows.Forms.DataGridView();
            this.colAtributoRecepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVolorReceptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgwConseptos = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFechaComprobante = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textConsecutivo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUsoCFDI = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbCodigosCont = new System.Windows.Forms.ComboBox();
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPoliza = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbFolioFiscal = new System.Windows.Forms.Label();
            this.txtFolioFiscal = new System.Windows.Forms.TextBox();
            this.colNoIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValorUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colImporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbCont = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cmbCodifoInterno = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabXml.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComprobante)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.gbExEn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgExpEn)).BeginInit();
            this.gbDomFiscal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgDomFiscal)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwReceptor)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwConseptos)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolGuardar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1030, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolGuardar
            // 
            this.toolGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolGuardar.Image = ((System.Drawing.Image)(resources.GetObject("toolGuardar.Image")));
            this.toolGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolGuardar.Name = "toolGuardar";
            this.toolGuardar.Size = new System.Drawing.Size(23, 22);
            this.toolGuardar.Text = "Guardar";
            this.toolGuardar.Click += new System.EventHandler(this.toolGuardar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tabXml, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.07839F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.92161F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1030, 523);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // tabXml
            // 
            this.tabXml.Controls.Add(this.tabPage1);
            this.tabXml.Controls.Add(this.tabPage2);
            this.tabXml.Controls.Add(this.tabPage3);
            this.tabXml.Controls.Add(this.tabPage4);
            this.tabXml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabXml.Location = new System.Drawing.Point(3, 175);
            this.tabXml.Name = "tabXml";
            this.tabXml.SelectedIndex = 0;
            this.tabXml.Size = new System.Drawing.Size(1024, 345);
            this.tabXml.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvComprobante);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1016, 319);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Comprobante";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvComprobante
            // 
            this.dgvComprobante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComprobante.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAtributo,
            this.colValor});
            this.dgvComprobante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComprobante.Location = new System.Drawing.Point(3, 3);
            this.dgvComprobante.Name = "dgvComprobante";
            this.dgvComprobante.Size = new System.Drawing.Size(1010, 313);
            this.dgvComprobante.TabIndex = 0;
            // 
            // colAtributo
            // 
            this.colAtributo.HeaderText = "Atributo";
            this.colAtributo.Name = "colAtributo";
            // 
            // colValor
            // 
            this.colValor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colValor.HeaderText = "Valor";
            this.colValor.Name = "colValor";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gbExEn);
            this.tabPage2.Controls.Add(this.gbDomFiscal);
            this.tabPage2.Controls.Add(this.txtRegimenEmisor);
            this.tabPage2.Controls.Add(this.txtRfcRazonSocial);
            this.tabPage2.Controls.Add(this.txtRfcEmisor);
            this.tabPage2.Controls.Add(this.lbRegimen);
            this.tabPage2.Controls.Add(this.lbRazonSocial);
            this.tabPage2.Controls.Add(this.lbRFC);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1016, 319);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Emisor";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gbExEn
            // 
            this.gbExEn.Controls.Add(this.dvgExpEn);
            this.gbExEn.Location = new System.Drawing.Point(443, 72);
            this.gbExEn.Name = "gbExEn";
            this.gbExEn.Size = new System.Drawing.Size(430, 331);
            this.gbExEn.TabIndex = 7;
            this.gbExEn.TabStop = false;
            this.gbExEn.Text = "Expedido En";
            // 
            // dvgExpEn
            // 
            this.dvgExpEn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgExpEn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAtributoExpEn,
            this.colValorExpEn});
            this.dvgExpEn.Location = new System.Drawing.Point(0, 20);
            this.dvgExpEn.Name = "dvgExpEn";
            this.dvgExpEn.Size = new System.Drawing.Size(424, 305);
            this.dvgExpEn.TabIndex = 0;
            // 
            // colAtributoExpEn
            // 
            this.colAtributoExpEn.HeaderText = "Atributo";
            this.colAtributoExpEn.Name = "colAtributoExpEn";
            // 
            // colValorExpEn
            // 
            this.colValorExpEn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colValorExpEn.HeaderText = "Valor";
            this.colValorExpEn.Name = "colValorExpEn";
            // 
            // gbDomFiscal
            // 
            this.gbDomFiscal.Controls.Add(this.dvgDomFiscal);
            this.gbDomFiscal.Location = new System.Drawing.Point(7, 72);
            this.gbDomFiscal.Name = "gbDomFiscal";
            this.gbDomFiscal.Size = new System.Drawing.Size(430, 331);
            this.gbDomFiscal.TabIndex = 6;
            this.gbDomFiscal.TabStop = false;
            this.gbDomFiscal.Text = "Domicilio Fiscal";
            // 
            // dvgDomFiscal
            // 
            this.dvgDomFiscal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgDomFiscal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAtributoDomFis,
            this.colValorDomFiscal});
            this.dvgDomFiscal.Location = new System.Drawing.Point(7, 20);
            this.dvgDomFiscal.Name = "dvgDomFiscal";
            this.dvgDomFiscal.Size = new System.Drawing.Size(417, 305);
            this.dvgDomFiscal.TabIndex = 0;
            // 
            // colAtributoDomFis
            // 
            this.colAtributoDomFis.HeaderText = "Atributo";
            this.colAtributoDomFis.Name = "colAtributoDomFis";
            // 
            // colValorDomFiscal
            // 
            this.colValorDomFiscal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colValorDomFiscal.HeaderText = "Valor";
            this.colValorDomFiscal.Name = "colValorDomFiscal";
            // 
            // txtRegimenEmisor
            // 
            this.txtRegimenEmisor.Location = new System.Drawing.Point(103, 45);
            this.txtRegimenEmisor.Name = "txtRegimenEmisor";
            this.txtRegimenEmisor.Size = new System.Drawing.Size(501, 20);
            this.txtRegimenEmisor.TabIndex = 5;
            // 
            // txtRfcRazonSocial
            // 
            this.txtRfcRazonSocial.Location = new System.Drawing.Point(361, 19);
            this.txtRfcRazonSocial.Name = "txtRfcRazonSocial";
            this.txtRfcRazonSocial.Size = new System.Drawing.Size(243, 20);
            this.txtRfcRazonSocial.TabIndex = 4;
            // 
            // txtRfcEmisor
            // 
            this.txtRfcEmisor.Location = new System.Drawing.Point(103, 19);
            this.txtRfcEmisor.Name = "txtRfcEmisor";
            this.txtRfcEmisor.Size = new System.Drawing.Size(162, 20);
            this.txtRfcEmisor.TabIndex = 3;
            // 
            // lbRegimen
            // 
            this.lbRegimen.AutoSize = true;
            this.lbRegimen.Location = new System.Drawing.Point(47, 45);
            this.lbRegimen.Name = "lbRegimen";
            this.lbRegimen.Size = new System.Drawing.Size(49, 13);
            this.lbRegimen.TabIndex = 2;
            this.lbRegimen.Text = "Regimen";
            // 
            // lbRazonSocial
            // 
            this.lbRazonSocial.AutoSize = true;
            this.lbRazonSocial.Location = new System.Drawing.Point(282, 19);
            this.lbRazonSocial.Name = "lbRazonSocial";
            this.lbRazonSocial.Size = new System.Drawing.Size(73, 13);
            this.lbRazonSocial.TabIndex = 1;
            this.lbRazonSocial.Text = "Razón Social:";
            // 
            // lbRFC
            // 
            this.lbRFC.AutoSize = true;
            this.lbRFC.Location = new System.Drawing.Point(66, 19);
            this.lbRFC.Name = "lbRFC";
            this.lbRFC.Size = new System.Drawing.Size(31, 13);
            this.lbRFC.TabIndex = 0;
            this.lbRFC.Text = "RFC:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgwReceptor);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1016, 319);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Receptor";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgwReceptor
            // 
            this.dgwReceptor.AllowUserToAddRows = false;
            this.dgwReceptor.AllowUserToDeleteRows = false;
            this.dgwReceptor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwReceptor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAtributoRecepto,
            this.colVolorReceptor});
            this.dgwReceptor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwReceptor.Location = new System.Drawing.Point(0, 0);
            this.dgwReceptor.Name = "dgwReceptor";
            this.dgwReceptor.ReadOnly = true;
            this.dgwReceptor.Size = new System.Drawing.Size(1016, 319);
            this.dgwReceptor.TabIndex = 0;
            // 
            // colAtributoRecepto
            // 
            this.colAtributoRecepto.HeaderText = "Atributo";
            this.colAtributoRecepto.Name = "colAtributoRecepto";
            this.colAtributoRecepto.ReadOnly = true;
            // 
            // colVolorReceptor
            // 
            this.colVolorReceptor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colVolorReceptor.HeaderText = "Valor";
            this.colVolorReceptor.Name = "colVolorReceptor";
            this.colVolorReceptor.ReadOnly = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgwConseptos);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1016, 319);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Conceptos";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgwConseptos
            // 
            this.dgwConseptos.AllowUserToAddRows = false;
            this.dgwConseptos.AllowUserToDeleteRows = false;
            this.dgwConseptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwConseptos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNoIdentificacion,
            this.colDescripcion,
            this.colCantidad,
            this.colUnidad,
            this.colValorUnitario,
            this.colImporte,
            this.cmbCont,
            this.cmbCodifoInterno});
            this.dgwConseptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwConseptos.Location = new System.Drawing.Point(0, 0);
            this.dgwConseptos.Name = "dgwConseptos";
            this.dgwConseptos.Size = new System.Drawing.Size(1016, 319);
            this.dgwConseptos.TabIndex = 0;
            this.dgwConseptos.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwConseptos_CellValueChanged);
            this.dgwConseptos.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgwConseptos_EditingControlShowing);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFechaComprobante);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textConsecutivo);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtUsoCFDI);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtRazonSocial);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbCodigosCont);
            this.groupBox1.Controls.Add(this.dtFecha);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPoliza);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbFolioFiscal);
            this.groupBox1.Controls.Add(this.txtFolioFiscal);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1024, 166);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos:";
            // 
            // txtFechaComprobante
            // 
            this.txtFechaComprobante.Enabled = false;
            this.txtFechaComprobante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaComprobante.Location = new System.Drawing.Point(101, 108);
            this.txtFechaComprobante.Name = "txtFechaComprobante";
            this.txtFechaComprobante.Size = new System.Drawing.Size(501, 20);
            this.txtFechaComprobante.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Fecha CFDI:";
            // 
            // textConsecutivo
            // 
            this.textConsecutivo.Enabled = false;
            this.textConsecutivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textConsecutivo.Location = new System.Drawing.Point(769, 21);
            this.textConsecutivo.Name = "textConsecutivo";
            this.textConsecutivo.Size = new System.Drawing.Size(113, 20);
            this.textConsecutivo.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(655, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Número:";
            // 
            // txtUsoCFDI
            // 
            this.txtUsoCFDI.Enabled = false;
            this.txtUsoCFDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsoCFDI.Location = new System.Drawing.Point(101, 76);
            this.txtUsoCFDI.Name = "txtUsoCFDI";
            this.txtUsoCFDI.Size = new System.Drawing.Size(501, 20);
            this.txtUsoCFDI.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Uso de CFDI:";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Enabled = false;
            this.txtRazonSocial.Location = new System.Drawing.Point(101, 22);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(501, 20);
            this.txtRazonSocial.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Razon Social:";
            // 
            // cmbCodigosCont
            // 
            this.cmbCodigosCont.FormattingEnabled = true;
            this.cmbCodigosCont.Location = new System.Drawing.Point(769, 49);
            this.cmbCodigosCont.Name = "cmbCodigosCont";
            this.cmbCodigosCont.Size = new System.Drawing.Size(228, 21);
            this.cmbCodigosCont.TabIndex = 17;
            // 
            // dtFecha
            // 
            this.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecha.Location = new System.Drawing.Point(769, 105);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(113, 20);
            this.dtFecha.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(655, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Fecha:";
            // 
            // txtPoliza
            // 
            this.txtPoliza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPoliza.Location = new System.Drawing.Point(769, 78);
            this.txtPoliza.Name = "txtPoliza";
            this.txtPoliza.Size = new System.Drawing.Size(216, 20);
            this.txtPoliza.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(655, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Poliza:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(655, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Codigo Contable:";
            // 
            // lbFolioFiscal
            // 
            this.lbFolioFiscal.AutoSize = true;
            this.lbFolioFiscal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFolioFiscal.Location = new System.Drawing.Point(7, 50);
            this.lbFolioFiscal.Name = "lbFolioFiscal";
            this.lbFolioFiscal.Size = new System.Drawing.Size(71, 13);
            this.lbFolioFiscal.TabIndex = 10;
            this.lbFolioFiscal.Text = "Folio Fiscal";
            // 
            // txtFolioFiscal
            // 
            this.txtFolioFiscal.Enabled = false;
            this.txtFolioFiscal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolioFiscal.Location = new System.Drawing.Point(101, 50);
            this.txtFolioFiscal.Name = "txtFolioFiscal";
            this.txtFolioFiscal.Size = new System.Drawing.Size(501, 20);
            this.txtFolioFiscal.TabIndex = 11;
            // 
            // colNoIdentificacion
            // 
            this.colNoIdentificacion.HeaderText = "No Identificacion";
            this.colNoIdentificacion.Name = "colNoIdentificacion";
            this.colNoIdentificacion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colDescripcion
            // 
            this.colDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDescripcion.HeaderText = "Descripcion";
            this.colDescripcion.Name = "colDescripcion";
            // 
            // colCantidad
            // 
            this.colCantidad.HeaderText = "Cantiadad";
            this.colCantidad.Name = "colCantidad";
            // 
            // colUnidad
            // 
            this.colUnidad.HeaderText = "Unidad";
            this.colUnidad.Name = "colUnidad";
            // 
            // colValorUnitario
            // 
            this.colValorUnitario.HeaderText = "Valor Unitario";
            this.colValorUnitario.Name = "colValorUnitario";
            // 
            // colImporte
            // 
            this.colImporte.HeaderText = "Importe";
            this.colImporte.Name = "colImporte";
            // 
            // cmbCont
            // 
            this.cmbCont.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cmbCont.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.cmbCont.HeaderText = "Codigo contable";
            this.cmbCont.Name = "cmbCont";
            this.cmbCont.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cmbCont.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cmbCodifoInterno
            // 
            this.cmbCodifoInterno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cmbCodifoInterno.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.cmbCodifoInterno.HeaderText = "Codigo Interno";
            this.cmbCodifoInterno.Name = "cmbCodifoInterno";
            this.cmbCodifoInterno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frmFacRecibidas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 548);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "frmFacRecibidas";
            this.Text = "Facturas Recibidas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmFacRecibidas_FormClosing);
            this.Load += new System.EventHandler(this.frmFacRecibidas_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabXml.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvComprobante)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.gbExEn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgExpEn)).EndInit();
            this.gbDomFiscal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgDomFiscal)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgwReceptor)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgwConseptos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolGuardar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabXml;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtributo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValor;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox gbExEn;
        private System.Windows.Forms.DataGridView dvgExpEn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtributoExpEn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValorExpEn;
        private System.Windows.Forms.GroupBox gbDomFiscal;
        private System.Windows.Forms.DataGridView dvgDomFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtributoDomFis;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValorDomFiscal;
        private System.Windows.Forms.TextBox txtRegimenEmisor;
        private System.Windows.Forms.TextBox txtRfcRazonSocial;
        private System.Windows.Forms.TextBox txtRfcEmisor;
        private System.Windows.Forms.Label lbRegimen;
        private System.Windows.Forms.Label lbRazonSocial;
        private System.Windows.Forms.Label lbRFC;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgwReceptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtributoRecepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVolorReceptor;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgwConseptos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbCodigosCont;
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPoliza;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbFolioFiscal;
        private System.Windows.Forms.TextBox txtFolioFiscal;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsoCFDI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textConsecutivo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFechaComprobante;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNoIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValorUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn colImporte;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbCont;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbCodifoInterno;
    }
}