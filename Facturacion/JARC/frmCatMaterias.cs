﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Facturacion.JARC
{
    public partial class frmCatMaterias : Form
    {
        MySqlConnection conectar;
        int i = 0;
        int j = 0;
        public frmCatMaterias()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarCodigos();
        }

        private void BuscarCodigos()
        {
            DataTable dt = new DataTable();
            string query = string.Empty;

            dgvDatos.DataSource = null;

            query = "SELECT codMat, descripcionMat FROM catalogomateriales;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            //  conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            dgvDatos.DataSource = myAdapter.Fill(dt);
            conectar.Close();
            dgvDatos.DataSource = dt;
           
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void btnPreparar_Click(object sender, EventArgs e)
        {

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            string query = string.Empty;
            string strCodigo = string.Empty;
            string strCTA = string.Empty;
            string strSCTA = string.Empty;
            string strDescripcion = string.Empty;

            string strCodigoMod = string.Empty;
            string strDesMod = string.Empty;


            strCodigo = dgvDatos.Rows[i].Cells[0].Value.ToString();
            strDescripcion = dgvDatos.Rows[i].Cells[1].Value.ToString();

            strCodigoMod = txtCodigo.Text;
            strDesMod = rechDescrip.Text;

            query = "UPDATE  catalogomateriales  SET codMat = '" + strCodigoMod + "',descripcionMat = '" + strDesMod + "' where    codMat = '" + strCodigo + "'" ;
            conectar.Open();
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            mycomand.ExecuteReader();
            conectar.Close();
            MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BuscarCodigos();
        }

        private void frmCatMaterias_Load(object sender, EventArgs e)
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }

        private void dgvDatos_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            i = dgvDatos.CurrentCell.RowIndex;
            j = dgvDatos.CurrentCell.ColumnIndex;
            string strMayor = string.Empty;
            string strCta = string.Empty;
            string strSCTA = string.Empty;
            string strDescipcion = string.Empty;


            txtCodigo.Text = dgvDatos.Rows[i].Cells[0].Value.ToString();
            rechDescrip.Text = dgvDatos.Rows[i].Cells[1].Value.ToString();
        }
    }
}
