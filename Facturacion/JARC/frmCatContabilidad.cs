﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Facturacion.JARC
{
    public partial class frmCatContabilidad : Form
    {
        MySqlConnection conectar;
        int i = 0;
        int j = 0;
        public frmCatContabilidad()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ValidarDatos();
        }
        private void ValidarDatos()
        {
            if (txtCTA.Text == "" || txtSCTA.Text == "" || txtMayor.Text == "" || rechDescrip.Text == "")
            {

                MessageBox.Show("DEBE LLENAR TODOS LOS CAMPOS PARA GUARDAR UN CODIGO CONTABLE", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                GuardarCodigo();
            }
        }
        private void GuardarCodigo()
        {
            string query = string.Empty;
            string strMayor = string.Empty;
            string strCTA = string.Empty;
            string strSCTA = string.Empty;
            string strDescripcion = string.Empty;


            strMayor = txtMayor.Text;
            strCTA = txtCTA.Text;
            strSCTA = txtSCTA.Text;
            strDescripcion = rechDescrip.Text;

            query = "INSERT INTO CATCONTABILIDAD VALUE('" + strMayor + "'" + ",'" + strCTA + "'" + ",'" + strSCTA + "'" + ",'" + strDescripcion + "'" + ");";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            conectar.Open();
            mycomand.ExecuteReader();
            conectar.Close();
            MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BuscarCodigos();

        }

        private void frmCatContabilidad_Load(object sender, EventArgs e)
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarCodigos();
        }

        private void btnPreparar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        private void Limpiar()
        {
            txtSCTA.Clear();
            txtCTA.Clear();
            txtMayor.Clear();
            rechDescrip.Clear();
            dgvDatos.DataSource = null;
        }
        private void BuscarCodigos()
        {
            DataTable dt = new DataTable();
            string query = string.Empty;

            dgvDatos.DataSource = null;

            query = "SELECT MAYOR,CTA,SCTA,DESCRIPCION  FROM CATCONTABILIDAD;;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
          //  conectar.Open();
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            dgvDatos.DataSource = myAdapter.Fill(dt);
            conectar.Close();
            dgvDatos.DataSource = dt;
            //if (dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        dgvDatos.Rows[i].Cells[0].Value = dt.Rows[i].ItemArray[0];
            //        dgvDatos.Rows[i].Cells[1].Value = dt.Rows[i].ItemArray[1];
            //        dgvDatos.Rows[i].Cells[2].Value = dt.Rows[i].ItemArray[2];
            //        dgvDatos.Rows[i].Cells[3].Value = dt.Rows[i].ItemArray[3];
            //    }
            //}
            //else
            //{
            //   MessageBox.Show("No hay datos");
            // }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            string query = string.Empty;
            string strMayor = string.Empty;
            string strCTA = string.Empty;
            string strSCTA = string.Empty;
            string strDescripcion = string.Empty;

            string strMayorMod = string.Empty;
            string strCTAMod = string.Empty;
            string strSCTAMod = string.Empty;
            string strDescripcionMod = string.Empty;


            strMayor = dgvDatos.Rows[i].Cells[0].Value.ToString();
            strCTA = dgvDatos.Rows[i].Cells[1].Value.ToString();
            strSCTA = dgvDatos.Rows[i].Cells[2].Value.ToString();
            strDescripcion = dgvDatos.Rows[i].Cells[3].Value.ToString();

            strMayorMod = txtMayor.Text;
            strCTAMod = txtCTA.Text;
            strSCTAMod = txtSCTA.Text;
            strDescripcionMod = rechDescrip.Text;

            query = "UPDATE  CATCONTABILIDAD  SET MAYOR = '" + strMayorMod + "',CTA = '" + strCTAMod + "', SCTA = '" + strSCTAMod + "' , DESCRIPCION = '" + strDescripcionMod + "' where    MAYOR = '" + strMayor + "' AND CTA = '" + strCTA + "' AND SCTA = '" + strSCTA + "' AND DESCRIPCION = '" + strDescripcion + "'";
            conectar.Open();
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            mycomand.ExecuteReader();
            conectar.Close();
            MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BuscarCodigos();
        }

        private void dgvDatos_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            i = dgvDatos.CurrentCell.RowIndex;
            j = dgvDatos.CurrentCell.ColumnIndex;
            string strMayor = string.Empty;
            string strCta = string.Empty;
            string strSCTA = string.Empty;
            string strDescipcion = string.Empty;


            txtMayor.Text = dgvDatos.Rows[i].Cells[0].Value.ToString();
            txtCTA.Text = dgvDatos.Rows[i].Cells[1].Value.ToString();
            txtSCTA.Text = dgvDatos.Rows[i].Cells[2].Value.ToString();
            rechDescrip.Text = dgvDatos.Rows[i].Cells[3].Value.ToString();


        }
    }
}
