﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic.Devices;
using System.Xml;
using System.IO.Compression;
using Ionic.Zip;
using System.Xml.Serialization;
using Facturacion.Clases;

namespace Facturacion.JARC
{
    public partial class frmBuscarXML : Form
    {
        Computer mycomputer = new Computer();
        Factura facturaVo = new Factura();
        public frmBuscarXML()
        {
            InitializeComponent();
        }

        private void toolStripBuscar_Click(object sender, EventArgs e)
        {
            //backgroundWorker1.RunWorkerAsync();
            ValidarDatos();

        }
        private void ValidarDatos()
        {
            if (cmbTipoFactura.Text == "")
            {
                MessageBox.Show("DEBE SELECCIONAR UN TIPO DE FACTURA", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                BuscarXML();
            }
        }

        private void BuscarXML()
        {
            string strTest = string.Empty;
            string strTipo = string.Empty;
            string strRuta = string.Empty;
            string strCopiar = string.Empty;
            string strExtraer = string.Empty;
            string strFecha = string.Empty;
            string strPegar = string.Empty;
            string strAno = string.Empty;
            string strMes = string.Empty;
            string strVerificaRuta = string.Empty;
            int intCont = 0;
            strTipo = cmbTipoFactura.Text;

            if (strTipo == "EMITIDOS")
            {
                strTipo = "Facturas_Emitidas";
            }
            else if (strTipo == "RECIBIDOS")
            {
                strTipo = "Facturas_Recibidas";
            }

            //DirectoryInfo di = new DirectoryInfo(@"C:\\Users\\Esmeralda Ruiz\\Downloads\\Descarga_Facturas\\" + strTipo + "");
            DirectoryInfo di = new DirectoryInfo(@"C:\\CapDigi\\CFDiDescargaXml\\Emitidos\\\RACA490321B88");

            // strRuta = @"C:\\Users\\ruize\\Documents\\"+ strTipo;

            foreach (var fi in di.GetFiles())
            {
               // this.timer1.Start();
                //progressBar1.Minimum = 0;

                strTest = fi.Name;
                strCopiar = di + "\\" + strTest;
                DirectoryInfo r = new DirectoryInfo(strCopiar);

                ZipFile zip = ZipFile.Read(strCopiar);


                foreach (ZipEntry f in zip)
                {
                    

                    f.Extract(di.ToString(), ExtractExistingFileAction.OverwriteSilently);
                    strCopiar = @di + "\\" + f.FileName;

                    XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
                    XmlTextReader reader = new XmlTextReader(@"" + strCopiar);
                    Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
                    facturaVo.DatosFactura = factura;

                    if (facturaVo.DatosFactura.Emisor.Rfc == "RACA490321B88")
                    {
                        strFecha = facturaVo.DatosFactura.Fecha.ToString();
                        strAno = strFecha.Substring(6, 4);
                        strMes = strFecha.Substring(3, 2);

                        strVerificaRuta = "C:\\Users\\Esmeralda Ruiz\\Documents\\Facturas_Emitidas\\" + strAno + "\\" + strMes;

                        if (!(Directory.Exists(strVerificaRuta)))
                        {
                            Directory.CreateDirectory(strVerificaRuta);
                        }


                        //if (!System.IO.File.Exists(strVerificaRuta))
                        //    {
                        //        using (System.IO.FileStream fs = System.IO.File.Create(strVerificaRuta))
                        //        { }
                        //    }

                                    strPegar = "C:\\Users\\Esmeralda Ruiz\\Documents\\Facturas_Emitidas\\" + strAno + "\\" + strMes + "\\" + f.FileName;
                        reader.Close();
                        if (mycomputer.FileSystem.FileExists(strPegar) == false)
                        {
                            mycomputer.FileSystem.MoveFile(strCopiar, strPegar);
                        }
                        else
                        {
                            mycomputer.FileSystem.DeleteFile(strCopiar);
                        }
                    }
                    else
                    {
                        
                        strFecha = facturaVo.DatosFactura.Fecha.ToString();
                        strAno = strFecha.Substring(6, 4);
                        strMes = strFecha.Substring(3, 2);
                        reader.Close();
                        //strVerificaRuta = "C:\\Users\\ruize\\Documents\\Facturas_Emitidas\\" + strAno + "-" + strMes;
                        //if (!(Directory.Exists(strVerificaRuta)))
                        //{
                        //    Directory.CreateDirectory(strVerificaRuta);
                        //}

                        strPegar = "C:\\Users\\Esmeralda Ruiz\\Documents\\Facturas_Recibidas\\" + strAno + "-" + strMes + "\\" + f.FileName;
                        if (mycomputer.FileSystem.FileExists(strPegar) == false)
                        {
                            mycomputer.FileSystem.MoveFile(strCopiar, strPegar);
                        }
                        else
                        {
                            mycomputer.FileSystem.DeleteFile(strCopiar);
                        }
                        // mycomputer.FileSystem.MoveFile(strCopiar, strPegar);
                        //  mycomputer.FileSystem.DeleteFile(strCopiar);
                    }

                }
                intCont++;
               // progressBar1.MinimumSize(intCont);

            }
            
            MessageBox.Show("FACTURAS ACTUALIZADAS", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
           
          //  this.timer1.Stop();
        }


        //public static void Decompress(FileInfo fi)
        //{
        //    // Get the stream of the source file.
        //    using (FileStream inFile = fi.OpenRead())
        //    {
        //        // Get original file extension, for example
        //        // "doc" from report.doc.gz.
        //        string curFile = fi.FullName;
        //        string origName = curFile.Remove(curFile.Length -
        //                fi.Extension.Length);

        //        //Create the decompressed file.
        //        using (FileStream outFile = File.Create(origName))
        //        {
        //            using (GZipStream Decompress = new GZipStream(inFile,
        //                    CompressionMode.Decompress))
        //            {
        //                // Copy the decompression stream 
        //                // into the output file.
        //                Decompress.CopyTo(outFile);

        //                Console.WriteLine("Decompressed: {0}", fi.Name);

        //            }
        //        }
        //    }
        //}
        private void frmBuscarXML_Load(object sender, EventArgs e)
        {
            CargarCombo();
        }
        private void CargarCombo()
        {
            cmbTipoFactura.Items.Add("EMITIDOS");
            cmbTipoFactura.Items.Add("RECIBIDOS");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.progressBar1.Increment(1);
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
           
        }
    }
}
