﻿namespace Facturacion.JARC
{
    partial class frmDescargaFacturas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDescargaFacturas));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolDescargaFac = new System.Windows.Forms.ToolStripButton();
            this.toolDescarga = new System.Windows.Forms.ToolStripButton();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolDescargaFac,
            this.toolDescarga});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(624, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolDescargaFac
            // 
            this.toolDescargaFac.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDescargaFac.Image = ((System.Drawing.Image)(resources.GetObject("toolDescargaFac.Image")));
            this.toolDescargaFac.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDescargaFac.Name = "toolDescargaFac";
            this.toolDescargaFac.Size = new System.Drawing.Size(23, 22);
            this.toolDescargaFac.Text = "Descarga de Facturas";
            this.toolDescargaFac.Visible = false;
            this.toolDescargaFac.Click += new System.EventHandler(this.toolDescargaFac_Click);
            // 
            // toolDescarga
            // 
            this.toolDescarga.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDescarga.Image = ((System.Drawing.Image)(resources.GetObject("toolDescarga.Image")));
            this.toolDescarga.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDescarga.Name = "toolDescarga";
            this.toolDescarga.Size = new System.Drawing.Size(23, 22);
            this.toolDescarga.Text = "toolStripButton1";
            this.toolDescarga.Visible = false;
            this.toolDescarga.Click += new System.EventHandler(this.toolDescarga_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 25);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(624, 305);
            this.webBrowser1.TabIndex = 2;
            this.webBrowser1.FileDownload += new System.EventHandler(this.webBrowser1_FileDownload);
            this.webBrowser1.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser1_Navigating);
            // 
            // frmDescargaFacturas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 330);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "frmDescargaFacturas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DESCARGA DE FACTURAS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmDescargaFacturas_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolDescargaFac;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ToolStripButton toolDescarga;
    }
}