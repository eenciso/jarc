﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Facturacion.Clases;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Facturacion.JARC
{
    public partial class frmEmitidas : Form
    {
        MySqlConnection conectar;
        Factura facturaVo = new Factura();
        private string srrUUID;
        string strAnioGeneral = string.Empty;
        string strMesGeneral = string.Empty;
        public frmEmitidas(string strAnioGeneral1, string strMesGeneral1,string strval)
        {
            strAnioGeneral = strAnioGeneral1;
            strMesGeneral = strMesGeneral1;
            InitializeComponent();
        }

        private void CargarCombo()
        {
            string mes = string.Empty;
            DataSet ds = new DataSet();
            string query = string.Empty;

            query = "SELECT *  FROM MESES;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds, "MES");

            foreach (DataRow row in ds.Tables["MES"].Rows)
            {
                cmbMes.Items.Add(row.ItemArray[0] + "-" + row.ItemArray[1]);
            }
        }

        private void frmEmitidas_Load(object sender, EventArgs e)
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
                CargarCombo();
                CargarAnio();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }

        private void CargarAnio()
        {
            DateTime anio = DateTime.Today;

            for (int i = anio.Year; i >= 1900; i--)
            {
                cmbAnio.Items.Add(i);
            }

        }

        private void toolStripBuscar_Click(object sender, EventArgs e)
        {
            ValidarCampos();
        }

        private void ValidarCampos()
        {
            if (strMesGeneral != "")
            {
                BuscarDatos();
            }
            else if (cmbMes == null || cmbAnio == null)
            {

                //dgbRecibidos.DataSource = null;
                MessageBox.Show("DEBE INGRESAR EL MES Y EL AÑO", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (cmbMes.Text == "" || cmbAnio.Text == "")
            {
                MessageBox.Show("DEBE INGRESAR EL MES Y EL AÑO", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                BuscarDatos();
            }
            //if (cmbMes.Text == "" || cmbAnio.Text == "")
            //{
            //    MessageBox.Show("DEBE INGRESAR EL MES Y EL AÑO", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            //else
            //{
            //    BuscarFacturasEmitidas();
            //    AgregarConseutivo();
            //    SumarTotales();
            //    BuscarPoliza();
            //}
        }

        private void BuscarDatos()
        {
            BuscarFacturasEmitidas();
            AgregarConseutivo();
            SumarTotales();
            BuscarPoliza();
        }

        private void BuscarFacturasEmitidas()
        {
            int i = 0;
            string strMes = string.Empty;
            string strAno = string.Empty;
            string strMeses = string.Empty;
            string strAnoS = string.Empty;
            string strTest = string.Empty;
            string strFecha = string.Empty;
            string strHora = string.Empty;
            string strDia = string.Empty;
            string strFolio = string.Empty;
            string strNombre = string.Empty;
            string strImpuestos = string.Empty;
            List<string> listUUID = new List<string>();
            //strMes = cmbMes.Text.ToString();
            //strMes = strMes.Substring(0, 2);
            //strAno = cmbAnio.Text.ToString();
            if (strAnioGeneral != "" && strMesGeneral != "")
            {
                strMes = strAnioGeneral;
                strAno = strMesGeneral.Substring(0, 2);
                strAno = strAno + "-" + strMes;
            }
            else
            {
                strMes = cmbMes.Text.ToString();
                strMes = strMes.Substring(0, 2);
                strAno = cmbAnio.Text.ToString();
                strAno = strAno + "-" + strMes;
            }
            //DirectoryInfo di = new DirectoryInfo(@"C:\\Users\\Jazmin\\OneDrive\\Documentos\\Facturas_Emitidas\\" + strAno + "\\" + strMes);
            DirectoryInfo di = new DirectoryInfo(@"C:\\CapDigi\\CFDiDescargaXml\\Emitidos\\RACA490321B88\\" + strAno );

            foreach (var fi in di.GetFiles())
            {
                strTest = fi.Name;
                XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
                XmlTextReader reader = new XmlTextReader(@"" + di + "\\" + strTest + "");
                Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
                facturaVo.DatosFactura = factura;

                strFecha = facturaVo.DatosFactura.Fecha.ToString().Replace('/', '-');
                strAnoS = strFecha.Substring(6, 4);
                strMeses = strFecha.Substring(3, 2);
                strDia = strFecha.Substring(0, 2);
                strHora = strFecha.Substring(11, 8);

                if (strMes.Equals(strMeses))
                {
                    if (lblOk.Text != "ok")
                        dgbEmitidos.Rows.Add();
                    strFolio = facturaVo.DatosFactura.Folio;
                    strNombre = facturaVo.DatosFactura.Receptor.Nombre;
                    if (facturaVo.DatosFactura.Impuestos != null)
                    {
                        strImpuestos = facturaVo.DatosFactura.Impuestos.TotalImpuestosTrasladados.ToString();
                    }
                    else
                    {
                        strImpuestos = "";
                    }

                    if (strFolio == null)
                        strFolio = "";
                    if (strNombre == null)
                        strNombre = "";
                    if (strImpuestos == null)
                        strImpuestos = "";

                    dgbEmitidos.Rows[i].Cells[1].Value = strFolio;
                    dgbEmitidos.Rows[i].Cells[2].Value = strAno + "-" + strMeses + "-" + strDia + " " + strHora;
                    dgbEmitidos.Rows[i].Cells[3].Value = facturaVo.DatosFactura.Receptor.Rfc.ToString();
                    dgbEmitidos.Rows[i].Cells[4].Value = strNombre;
                    dgbEmitidos.Rows[i].Cells[5].Value = facturaVo.DatosFactura.SubTotal.ToString();
                    dgbEmitidos.Rows[i].Cells[6].Value = strImpuestos;
                    dgbEmitidos.Rows[i].Cells[7].Value = facturaVo.DatosFactura.Total.ToString();
                    dgbEmitidos.Rows[i].Cells[17].Value = strTest;
                    i++;
                }
                else
                {
                    listUUID.Add(strTest);
                }
            }
            if (listUUID.Count > 0)
            {
                i++;
                dgbEmitidos.Rows.Add();
                foreach (string s in listUUID)
                {

                    XmlSerializer serielizer = new XmlSerializer(typeof(Comprobante));
                    XmlTextReader reader = new XmlTextReader(@"" + di + "\\" + s + "");
                    Comprobante factura = (Comprobante)serielizer.Deserialize(reader);
                    facturaVo.DatosFactura = factura;

                    strFecha = facturaVo.DatosFactura.Fecha.ToString().Replace('/', '-');
                    strAnoS = strFecha.Substring(6, 4);
                    strMeses = strFecha.Substring(3, 2);
                    strDia = strFecha.Substring(0, 2);
                    strHora = strFecha.Substring(11, 8);

                    dgbEmitidos.Rows.Add();
                    strFolio = facturaVo.DatosFactura.Folio;
                    strNombre = facturaVo.DatosFactura.Receptor.Nombre;
                    if (facturaVo.DatosFactura.Impuestos != null)
                    {
                        strImpuestos = facturaVo.DatosFactura.Impuestos.TotalImpuestosTrasladados.ToString();
                    }
                    else
                    {
                        strImpuestos = "";
                    }
                    if (strFolio == null)
                        strFolio = "";
                    if (strNombre == null)
                        strNombre = "";
                    if (strImpuestos == null)
                        strImpuestos = "";

                    dgbEmitidos.Rows[i].Cells[1].Value = strFolio;
                    dgbEmitidos.Rows[i].Cells[2].Value = strAno + "-" + strMeses + "-" + strDia + " " + strHora;
                    dgbEmitidos.Rows[i].Cells[3].Value = facturaVo.DatosFactura.Receptor.Rfc.ToString();
                    dgbEmitidos.Rows[i].Cells[4].Value = strNombre;
                    dgbEmitidos.Rows[i].Cells[5].Value = facturaVo.DatosFactura.SubTotal.ToString();
                    dgbEmitidos.Rows[i].Cells[6].Value = strImpuestos;
                    dgbEmitidos.Rows[i].Cells[7].Value = facturaVo.DatosFactura.Total.ToString();
                    dgbEmitidos.Rows[i].Cells[17].Value = strTest;
                    i++;
                }
            }
            lblTotal.Text = dgbEmitidos.Rows.Count.ToString();
        }
        private void ExtraerComponente(int i)
        {
            dgbEmitidos.Rows.Add();
            dgbEmitidos.Rows[i].Cells[1].Value = facturaVo.DatosFactura.Folio.ToString();
        }

        private void dgbEmitidos_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            string strAno = string.Empty;
            string strMes = string.Empty;
            string strFecha = string.Empty;
            string strUUID = string.Empty;
            string strUUID1 = string.Empty;
            string strRuta = string.Empty;
            string strPoliza = string.Empty;
            int i = dgbEmitidos.CurrentCell.RowIndex;
            int j = dgbEmitidos.CurrentCell.ColumnIndex;
            string strConsecutivo = string.Empty;
            string strClenite = string.Empty;

            if (dgbEmitidos.Rows[i].Cells[17].Value != null)
            {
                strUUID = dgbEmitidos.Rows[i].Cells[17].Value.ToString();
                strFecha = dgbEmitidos.Rows[i].Cells[2].Value.ToString();
                strAno = strFecha.Substring(0, 4);
                strMes = strFecha.Substring(5, 2);
                strRuta = strAno + "-" + strMes + "\\" + strUUID;
                if(dgbEmitidos.Rows[i].Cells[15].Value != null)
                strPoliza = dgbEmitidos.Rows[i].Cells[15].Value.ToString();
                strConsecutivo = dgbEmitidos.Rows[i].Cells[0].Value.ToString();

                strClenite = dgbEmitidos.Rows[i].Cells[4].Value.ToString();

                frmFacturasEmitidas frmFeM = new frmFacturasEmitidas(strRuta, strUUID, strPoliza, strConsecutivo, strClenite, cmbAnio.Text,  cmbAnio.Text);
                DialogResult result = frmFeM.ShowDialog();
                if (result == DialogResult.OK)
                {

                }
                else
                {
                    lblOk.Text = "ok";
                    toolStripBuscar_Click(null, null);
                }
            }
        }

        private void toolStripGuardar_Click(object sender, EventArgs e)
        {
            if (dgbEmitidos.Rows.Count > 0)
            {
                GuardarEmitidas();
                GuardarTotalMensual();
            }
            else
            {
                MessageBox.Show("DEDE REALIZAR UNA BUSQUEDA", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void GuardarEmitidas()
        {
            string query = string.Empty;
            string querys = string.Empty;
            string strConsulta = string.Empty;
            string srrUUID = string.Empty;
            string strNombre = string.Empty;
            string strIVA = string.Empty;
            string strConsecutivo = string.Empty;

            for (int i = 0; i < dgbEmitidos.Rows.Count; i++)
            {
                if (dgbEmitidos.Rows[i].Cells[2].Value != null)
                {

                    if (dgbEmitidos.Rows[i].Cells[6].Value != null)
                        strConsecutivo = dgbEmitidos.Rows[i].Cells[0].Value.ToString();

                    strNombre = dgbEmitidos.Rows[i].Cells[4].Value.ToString();
                    strIVA = dgbEmitidos.Rows[i].Cells[6].Value.ToString();
                    if (strNombre == null)
                        strNombre = "";

                    if (strIVA == null || strIVA == "")
                        strIVA = "0";

                    strConsulta = "SELECT UUID  FROM FACEMITIDAS WHERE UUID ='" + dgbEmitidos.Rows[i].Cells[17].Value.ToString() + "';";

                    MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
                    MySqlDataReader myreader = mycomands.ExecuteReader();

                    if (myreader.Read())
                        srrUUID = myreader["UUID"].ToString();

                    if (srrUUID == "")
                    {
                        querys = "INSERT INTO FACEMITIDAS " + "VALUE(" + strConsecutivo + ",'" + dgbEmitidos.Rows[i].Cells[1].Value.ToString() + "','" + dgbEmitidos.Rows[i].Cells[2].Value.ToString() + "','" + dgbEmitidos.Rows[i].Cells[3].Value.ToString() + "','" + strNombre + "'," + dgbEmitidos.Rows[i].Cells[5].Value.ToString() + "," + strIVA + "," + dgbEmitidos.Rows[i].Cells[7].Value.ToString() + ",'" + dgbEmitidos.Rows[i].Cells[17].Value.ToString() + "'" + ",''" + ");";
                        query = querys + query;
                    }
                    myreader.Close();
                }

            }
            if (query != "")
            {
                MySqlCommand mycomand = new MySqlCommand(query, conectar);
                MySqlDataReader myreader = mycomand.ExecuteReader();
                myreader.Close();
                //  MessageBox.Show("LAS FACTURAS SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //else
            //{
            //    GuardarTotalMensual();
            //    MessageBox.Show("LAS FACTURAS YA SE GUARDARON", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void toolStripLimpiar_Click(object sender, EventArgs e)
        {
            dgbEmitidos.DataSource = null;
            dgbEmitidos.Rows.Clear();
            cmbMes.SelectedIndex = -1;
            cmbAnio.SelectedIndex = -1;
        }

        private void AgregarConseutivo()
        {
            int intContador = 1;
            for (int i = 0; i < dgbEmitidos.Rows.Count; i++)
            {
                if (i == 0)
                {
                    dgbEmitidos.Rows[i].Cells[0].Value = "1";
                    intContador++;
                }
                else if (dgbEmitidos.Rows[i].Cells[2].Value == null)
                {
                    // intContador = i - 1;
                    dgbEmitidos.Rows[i].Cells[0].Value = "";

                }
                else
                {
                    //  intContador = i;
                    dgbEmitidos.Rows[i].Cells[0].Value = intContador;
                    intContador++;

                }
                //else if (dgbEmitidos.Rows[i].Cells[2].Value == null)
                //{
                //    dgbEmitidos.Rows[i].Cells[0].Value = "";
                //}
            }
        }
        private void SumarTotales()
        {
            double dSubtotal = 0;
            double dIVA = 0;
            double dTotal = 0;
            double dSumaSubtotal = 0;
            double dSumaIVA = 0;
            double dSumaTotal = 0;
            string strTotal = string.Empty;
            string strIVA = string.Empty;
            string strSubtotal = string.Empty;
            int intCont = 0;
            for (int i = 0; i < dgbEmitidos.Rows.Count; i++)
            {
                intCont = i;
                // strTotal = dgbEmitidos.Rows[i].Cells[5].Value.ToString();
                // if (strTotal == null || strTotal == "")
                if (dgbEmitidos.Rows[i].Cells[5].Value == null || dgbEmitidos.Rows[i].Cells[5].Value.ToString() == "")
                {
                    strTotal = "0";
                }
                else
                {
                    strTotal = dgbEmitidos.Rows[i].Cells[5].Value.ToString();
                }
                // strIVA = dgbEmitidos.Rows[i].Cells[6].Value.ToString();
                // if (strIVA == null || strIVA == "")
                if (dgbEmitidos.Rows[i].Cells[6].Value == null || dgbEmitidos.Rows[i].Cells[6].Value.ToString() == "")
                {
                    strIVA = "0";
                }
                else
                {
                    strIVA = dgbEmitidos.Rows[i].Cells[6].Value.ToString();
                }
                //  strSubtotal = dgbEmitidos.Rows[i].Cells[7].Value.ToString();
                // if (strSubtotal == null || strSubtotal == "")
                if (dgbEmitidos.Rows[i].Cells[7].Value == null || dgbEmitidos.Rows[i].Cells[7].Value.ToString() == "")
                {
                    strSubtotal = "0";
                }
                else
                {
                    strSubtotal = dgbEmitidos.Rows[i].Cells[7].Value.ToString();
                }

                dSubtotal = Convert.ToDouble(strTotal);
                dIVA = Convert.ToDouble(strIVA);
                dTotal = Convert.ToDouble(strSubtotal);

                dSumaSubtotal = dSumaSubtotal + dSubtotal;
                dSumaIVA = dSumaIVA + dIVA;
                dSumaTotal = dSumaTotal + dTotal;
                if (dgbEmitidos.Rows[i].Cells[2].Value == null)
                {
                    dgbEmitidos.Rows[i].Cells[12].Value = dSumaSubtotal;
                    dgbEmitidos.Rows[i].Cells[13].Value = dSumaIVA;
                    dgbEmitidos.Rows[i].Cells[14].Value = dSumaTotal;

                    return;
                }

            }

            dgbEmitidos.Rows[intCont].Cells[12].Value = dSumaSubtotal;
            dgbEmitidos.Rows[intCont].Cells[13].Value = dSumaIVA;
            dgbEmitidos.Rows[intCont].Cells[14].Value = dSumaTotal;
        }

        private void GuardarTotalMensual()
        {
            string querys = string.Empty;
            string strMes = string.Empty;
            string strAno = string.Empty;
            string strConsulta = string.Empty;
            string strCont = string.Empty;
            strMes = cmbMes.Text.Substring(0, 2);
            strAno = cmbAnio.Text;
            for (int i = 0; i < dgbEmitidos.Rows.Count; i++)
            {
                if (dgbEmitidos.Rows[i].Cells[12].Value != null)
                {
                    // conectar.Close();
                    //ACOMODAR 
                    strConsulta = "SELECT ANO  FROM INGRESOSEGRESOSMENSUALES WHERE MES = " + strMes + " AND MENSUAL = 'E' AND ANO = " + strAno + "; ";

                    MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
                    MySqlDataReader myreader = mycomands.ExecuteReader();

                    if (myreader.Read())
                    // if (strCont != "")
                    {
                        strCont = myreader["ANO"].ToString();
                        if (strCont != "")
                            querys = "UPDATE INGRESOSEGRESOSMENSUALES SET IMPORTE = '" + dgbEmitidos.Rows[i].Cells[12].Value.ToString() + "',  IVA = '" + dgbEmitidos.Rows[i].Cells[13].Value.ToString() + "', TOTAL = '" + dgbEmitidos.Rows[i].Cells[14].Value.ToString() + "'  WHERE ANO =" + strAno + " AND MES = '" + strMes + "' AND MENSUAL = 'E'";
                    }
                    else
                    {
                        querys = "INSERT INTO INGRESOSEGRESOSMENSUALES " + "VALUE(" + cmbAnio.Text + ",'" + strMes + "','" + dgbEmitidos.Rows[i].Cells[12].Value.ToString() + "'," + "''" + ",'" + dgbEmitidos.Rows[i].Cells[13].Value.ToString() + "','" + dgbEmitidos.Rows[i].Cells[14].Value.ToString() + "','E'" + ");";
                    }
                    myreader.Close();

                    // MySqlCommand mycomand = new MySqlCommand(querys, conectar);
                    //  mycomand.ExecuteReader();

                    MySqlCommand mycomand = new MySqlCommand(querys, conectar);
                    MySqlDataReader myreader1 = mycomand.ExecuteReader();
                    myreader1.Close();
                    MessageBox.Show("DATOS GUARDADOS", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                //else
                //{
                //    return;
                //}
            }

        }
        private void BuscarPoliza()
        {
            string strConsulta = string.Empty;
            string strUUID = string.Empty;
            string strPoliza = string.Empty;
            int intI = 0;
            for (int i = 0; i < dgbEmitidos.Rows.Count; i++)
            {
                if (dgbEmitidos.Rows[i].Cells[17].Value != null)
                {
                    srrUUID = dgbEmitidos.Rows[i].Cells[17].Value.ToString();

                    strConsulta = "SELECT CODCONT  FROM FACEMITIDAS WHERE UUID ='" + srrUUID + "';";

                    MySqlCommand mycomands = new MySqlCommand(strConsulta, conectar);
                    MySqlDataReader myreader = mycomands.ExecuteReader();

                    intI = i;
                    if (myreader.Read())
                    {
                        strPoliza = myreader["CODCONT"].ToString();
                        dgbEmitidos.Rows[intI].Cells[15].Value = strPoliza;
                        if (strPoliza != "")
                        {
                            dgbEmitidos.Rows[i].DefaultCellStyle.BackColor = Color.Aquamarine;
                        }

                    }
                    myreader.Close();
                }
            }
        }
    }
}
