﻿namespace Facturacion.JARC
{
    partial class frmRecibidas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRecibidas));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripBuscar = new System.Windows.Forms.ToolStripButton();
            this.toolStripLimpiar = new System.Windows.Forms.ToolStripButton();
            this.toolStripGuardar = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgbRecibidos = new System.Windows.Forms.DataGridView();
            this.rowNu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowFolio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowFecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowRfc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowRazonSocial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowSubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowIva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowEstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowImporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowCredito = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowTotal1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowImporte1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowIVA1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowTotal2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowPoliza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowDiferiencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbAnio = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbMes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOk = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgbRecibidos)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBuscar,
            this.toolStripLimpiar,
            this.toolStripGuardar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(789, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripBuscar
            // 
            this.toolStripBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBuscar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBuscar.Image")));
            this.toolStripBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBuscar.Name = "toolStripBuscar";
            this.toolStripBuscar.Size = new System.Drawing.Size(23, 22);
            this.toolStripBuscar.Text = "Buscar";
            this.toolStripBuscar.Click += new System.EventHandler(this.toolStripBuscar_Click);
            // 
            // toolStripLimpiar
            // 
            this.toolStripLimpiar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLimpiar.Image")));
            this.toolStripLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLimpiar.Name = "toolStripLimpiar";
            this.toolStripLimpiar.Size = new System.Drawing.Size(23, 22);
            this.toolStripLimpiar.Text = "Limpiar";
            this.toolStripLimpiar.Click += new System.EventHandler(this.toolStripLimpiar_Click);
            // 
            // toolStripGuardar
            // 
            this.toolStripGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripGuardar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGuardar.Image")));
            this.toolStripGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGuardar.Name = "toolStripGuardar";
            this.toolStripGuardar.Size = new System.Drawing.Size(23, 22);
            this.toolStripGuardar.Text = "Guardar";
            this.toolStripGuardar.Click += new System.EventHandler(this.toolStripGuardar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.dgbRecibidos, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.64286F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.35714F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(789, 368);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // dgbRecibidos
            // 
            this.dgbRecibidos.AllowUserToAddRows = false;
            this.dgbRecibidos.AllowUserToDeleteRows = false;
            this.dgbRecibidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgbRecibidos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgbRecibidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgbRecibidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowNu,
            this.rowFolio,
            this.rowFecha,
            this.rowRfc,
            this.rowRazonSocial,
            this.rowSubTotal,
            this.rowIva,
            this.rowTotal,
            this.rowEstatus,
            this.rowImporte,
            this.rowCredito,
            this.rowTotal1,
            this.rowImporte1,
            this.rowIVA1,
            this.rowTotal2,
            this.rowPoliza,
            this.rowDiferiencia,
            this.rowUID});
            this.dgbRecibidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgbRecibidos.Location = new System.Drawing.Point(3, 75);
            this.dgbRecibidos.Name = "dgbRecibidos";
            this.dgbRecibidos.ReadOnly = true;
            this.dgbRecibidos.Size = new System.Drawing.Size(783, 290);
            this.dgbRecibidos.TabIndex = 9;
            this.dgbRecibidos.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgbEmitidos_CellMouseUp);
            // 
            // rowNu
            // 
            this.rowNu.HeaderText = "Número";
            this.rowNu.Name = "rowNu";
            this.rowNu.ReadOnly = true;
            this.rowNu.Width = 69;
            // 
            // rowFolio
            // 
            this.rowFolio.HeaderText = "Folio";
            this.rowFolio.Name = "rowFolio";
            this.rowFolio.ReadOnly = true;
            this.rowFolio.Width = 54;
            // 
            // rowFecha
            // 
            this.rowFecha.HeaderText = "Fecha";
            this.rowFecha.Name = "rowFecha";
            this.rowFecha.ReadOnly = true;
            this.rowFecha.Width = 62;
            // 
            // rowRfc
            // 
            this.rowRfc.HeaderText = "RFC";
            this.rowRfc.Name = "rowRfc";
            this.rowRfc.ReadOnly = true;
            this.rowRfc.Width = 53;
            // 
            // rowRazonSocial
            // 
            this.rowRazonSocial.HeaderText = "Razon Social";
            this.rowRazonSocial.Name = "rowRazonSocial";
            this.rowRazonSocial.ReadOnly = true;
            this.rowRazonSocial.Width = 95;
            // 
            // rowSubTotal
            // 
            this.rowSubTotal.HeaderText = "SubTotal";
            this.rowSubTotal.Name = "rowSubTotal";
            this.rowSubTotal.ReadOnly = true;
            this.rowSubTotal.Width = 75;
            // 
            // rowIva
            // 
            this.rowIva.HeaderText = "I.V.A.";
            this.rowIva.Name = "rowIva";
            this.rowIva.ReadOnly = true;
            this.rowIva.Width = 58;
            // 
            // rowTotal
            // 
            this.rowTotal.HeaderText = "Total";
            this.rowTotal.Name = "rowTotal";
            this.rowTotal.ReadOnly = true;
            this.rowTotal.Width = 56;
            // 
            // rowEstatus
            // 
            this.rowEstatus.HeaderText = "Estatus";
            this.rowEstatus.Name = "rowEstatus";
            this.rowEstatus.ReadOnly = true;
            this.rowEstatus.Width = 67;
            // 
            // rowImporte
            // 
            this.rowImporte.HeaderText = "Importe";
            this.rowImporte.Name = "rowImporte";
            this.rowImporte.ReadOnly = true;
            this.rowImporte.Width = 67;
            // 
            // rowCredito
            // 
            this.rowCredito.HeaderText = "Credito";
            this.rowCredito.Name = "rowCredito";
            this.rowCredito.ReadOnly = true;
            this.rowCredito.Width = 65;
            // 
            // rowTotal1
            // 
            this.rowTotal1.HeaderText = "Total";
            this.rowTotal1.Name = "rowTotal1";
            this.rowTotal1.ReadOnly = true;
            this.rowTotal1.Width = 56;
            // 
            // rowImporte1
            // 
            this.rowImporte1.HeaderText = "IMPORTE";
            this.rowImporte1.Name = "rowImporte1";
            this.rowImporte1.ReadOnly = true;
            this.rowImporte1.Width = 81;
            // 
            // rowIVA1
            // 
            this.rowIVA1.HeaderText = "I.V.A.";
            this.rowIVA1.Name = "rowIVA1";
            this.rowIVA1.ReadOnly = true;
            this.rowIVA1.Width = 58;
            // 
            // rowTotal2
            // 
            this.rowTotal2.HeaderText = "TOTAL";
            this.rowTotal2.Name = "rowTotal2";
            this.rowTotal2.ReadOnly = true;
            this.rowTotal2.Width = 67;
            // 
            // rowPoliza
            // 
            this.rowPoliza.HeaderText = "Poliza";
            this.rowPoliza.Name = "rowPoliza";
            this.rowPoliza.ReadOnly = true;
            this.rowPoliza.Width = 60;
            // 
            // rowDiferiencia
            // 
            this.rowDiferiencia.HeaderText = "Diferiencia";
            this.rowDiferiencia.Name = "rowDiferiencia";
            this.rowDiferiencia.ReadOnly = true;
            this.rowDiferiencia.Width = 82;
            // 
            // rowUID
            // 
            this.rowUID.HeaderText = "UUID";
            this.rowUID.Name = "rowUID";
            this.rowUID.ReadOnly = true;
            this.rowUID.Width = 59;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblOk);
            this.groupBox1.Controls.Add(this.cmbAnio);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblTotal);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbMes);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(783, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos:";
            // 
            // cmbAnio
            // 
            this.cmbAnio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnio.FormattingEnabled = true;
            this.cmbAnio.Location = new System.Drawing.Point(230, 29);
            this.cmbAnio.Name = "cmbAnio";
            this.cmbAnio.Size = new System.Drawing.Size(116, 21);
            this.cmbAnio.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(355, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 24);
            this.label4.TabIndex = 19;
            this.label4.Text = "FACTURAS RECIBIDAS";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(677, 32);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(0, 13);
            this.lblTotal.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(581, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Total de facturas:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Año:";
            // 
            // cmbMes
            // 
            this.cmbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMes.FormattingEnabled = true;
            this.cmbMes.Location = new System.Drawing.Point(54, 29);
            this.cmbMes.Name = "cmbMes";
            this.cmbMes.Size = new System.Drawing.Size(116, 21);
            this.cmbMes.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mes:";
            // 
            // lblOk
            // 
            this.lblOk.AutoSize = true;
            this.lblOk.Location = new System.Drawing.Point(587, 50);
            this.lblOk.Name = "lblOk";
            this.lblOk.Size = new System.Drawing.Size(0, 13);
            this.lblOk.TabIndex = 22;
            // 
            // frmRecibidas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 393);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "frmRecibidas";
            this.Text = "Facturas Recibidas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRecibidas_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgbRecibidos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripBuscar;
        private System.Windows.Forms.ToolStripButton toolStripLimpiar;
        private System.Windows.Forms.ToolStripButton toolStripGuardar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dgbRecibidos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbMes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowNu;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowFolio;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowRfc;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowRazonSocial;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowSubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowIva;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowEstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowImporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowCredito;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowTotal1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowImporte1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowIVA1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowTotal2;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowPoliza;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowDiferiencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowUID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbAnio;
        private System.Windows.Forms.Label lblOk;
    }
}