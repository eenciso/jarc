﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Facturacion.Clases;

namespace Facturacion.JARC
{
    public partial class frmIngEgrMensuales : Form
    {
        MySqlConnection conectar;
        public frmIngEgrMensuales()
        {
            InitializeComponent();
        }

        private void frmIngEgrMensuales_Load(object sender, EventArgs e)
        {
            AgregarCombo();
            Conexion();
            CargarAnio();
        }

        private void Conexion()
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }
        private void AgregarCombo()
        {
            cmbTipo.Items.Add("EMITIDOS");
            cmbTipo.Items.Add("RECIBIDOS");
        }
        private void CargarAnio()
        {
            DateTime anio = DateTime.Today;

            for (int i = anio.Year; i >= 1900; i--)
            {
                cmbAnio.Items.Add(i);
            }

        }

        private void toolStripGuardar_Click(object sender, EventArgs e)
        {
            GuardarAcum();
        }
        private void ValidarDatos()
        {
            if (cmbAnio.Text == "" || cmbTipo.Text == "")
            {
                MessageBox.Show("DEDE REALIZAR UNA BUSQUEDA", "JARC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                BuscarReporteMensual();
            }
        }
        private void BuscarReporteMensual()
        {
            string strConsulta = string.Empty;
            string strAno = string.Empty;
            string strTipo = string.Empty;
            DataSet ds = new DataSet();

            if (cmbTipo.Text == "EMITIDOS")
            {
                strTipo = "E";
            }
            else if (cmbTipo.Text == "RECIBIDOS")
            {
                strTipo = "R";
            }

            strAno = cmbAnio.Text;

            strConsulta = "SELECT ANO,MES,IMPORTE,ACUM,IVA,TOTAL,MENSUAL  FROM INGRESOSEGRESOSMENSUALES WHERE ANO =" + strAno + " AND MENSUAL = '" + strTipo + "' ORDER BY MES;";
            MySqlDataAdapter mycomands = new MySqlDataAdapter(strConsulta, conectar);
            mycomands.Fill(ds);
            dgbTotalMedes.DataSource = ds.Tables[0];
            CalcularAcumulado();
        }

        private void toolStripBuscar_Click(object sender, EventArgs e)
        {
            ValidarDatos();
        }

        private void toolStripLimpiar_Click(object sender, EventArgs e)
        {
            dgbTotalMedes.DataSource = null;
            cmbAnio.Text = "";
            cmbTipo.SelectedIndex = -1;
        }
        private void CalcularAcumulado()
        {
            string strAcum1 = string.Empty;
            string strNuevo = string.Empty;
            decimal dSuma = 0;

            for (int i = 0; i < dgbTotalMedes.Rows.Count; i++)
            {
                if (i == 0)
                {
                    strAcum1 = dgbTotalMedes.Rows[i].Cells[2].Value.ToString();

                    dgbTotalMedes.Rows[i].Cells[3].Value = strAcum1;
                }
                else
                {
                    strNuevo = dgbTotalMedes.Rows[i].Cells[2].Value.ToString();
                    dSuma = Convert.ToDecimal(strNuevo) + Convert.ToDecimal(strAcum1);
                    dgbTotalMedes.Rows[i].Cells[3].Value = dSuma;
                    strAcum1 = dgbTotalMedes.Rows[i].Cells[3].Value.ToString();

                }
            }
        }

        private void GuardarAcum()
        {
            string querys = string.Empty;
            string strAno = string.Empty;
            string strMes = string.Empty;
            string strTipo = string.Empty;
            string strAcum = string.Empty;
            

            for (int i = 0; i < dgbTotalMedes.Rows.Count; i++)
            {
                strAno = dgbTotalMedes.Rows[i].Cells[0].Value.ToString();
                strMes = dgbTotalMedes.Rows[i].Cells[1].Value.ToString();
                strAcum = dgbTotalMedes.Rows[i].Cells[3].Value.ToString();
                strTipo = dgbTotalMedes.Rows[i].Cells[6].Value.ToString();

                querys = "UPDATE INGRESOSEGRESOSMENSUALES SET ACUM = '" + strAcum + "'  WHERE ANO =" + strAno + " AND MES = '" + strMes + "' AND MENSUAL = '" + strTipo + "';";
                MySqlCommand mycomand = new MySqlCommand(querys, conectar);
                MySqlDataReader myreader1 = mycomand.ExecuteReader();
                myreader1.Close();
                
            }
            MessageBox.Show("DATOS GUARDADOS", "JARC ", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

    }
}
