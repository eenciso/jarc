﻿using Facturacion.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Facturacion.JARC
{
    public partial class Form1 : Form
    {
        MySqlConnection conectar;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ConexionBD();
            CargarCodigoContables();
        }
        private void ConexionBD()
        {
            try
            {
                conectar = ConnectionString.ObtenerConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                return;
            }
        }
        private void CargarCodigoContables()
        {
            string query = string.Empty;
            DataTable ds = new DataTable();
            DataTable dt = new DataTable();
            query = "SELECT MAYOR,CTA,SCTA,DESCRIPCION  FROM CATCONTABILIDAD;";
            MySqlCommand mycomand = new MySqlCommand(query, conectar);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(mycomand);
            myAdapter.Fill(ds);

            dt.Columns.Add("MAYOR");
            dt.Columns.Add("CTA");
            dt.Columns.Add("SCTA");
            dt.Columns.Add("DESCRIPCION");

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                //  dt.Rows.Add(dt.Rows[i].ItemArray[0] + "-", row.ItemArray[1] + "-", row.ItemArray[2] + "-", row.ItemArray[3]);
                dt.Rows.Add(ds.Rows[i].ItemArray[0].ToString() + "-" + ds.Rows[i].ItemArray[1].ToString() + "-" + ds.Rows[i].ItemArray[2].ToString() + "-" + ds.Rows[i].ItemArray[3].ToString());

            }
            cmbCont.ValueMember = "MAYOR";
            cmbCont.DisplayMember = "MAYOR";
            DataRow topItem = dt.NewRow();
            //topItem[0] = 0;
            //topItem[1] = "selecciona";

            //txtUsoCFDI.Text = strUUID;
            dt.Rows.InsertAt(topItem, 0);
            cmbCont.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
          string str =  cmbCont.ValueMember;
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {

        }
    }
}
